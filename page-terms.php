<?php
/**
  Template Name: 本會簡介-組織條例
 */
global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

	


<?php
	$author_id = get_field('author');
	$cur = get_query_var('paged');

	$the_query = new WP_Query(array(
		'posts_per_page'=>20,
		'post_type' => 'org-term',
		'author_name' => $author_id,
		'paged' => $cur
	));
?>
			

		

<section class="container content-wrapper">
  <h3 class="content-title">組織條例</h3>
  <span class="star-symbol">★★★★★</span>
  <ul class="low-list">





    


	<?php while ($the_query -> have_posts()) : 
		$the_query -> the_post(); 
	?>

		

		<li>
			<a href="<?php echo get_permalink(); ?>" >
				<?php the_title(); ?>
			</a>
		</li>


	<?php
		endwhile;
		wp_reset_postdata(); 
	?>
  </ul>

<?php //set_query_var( 'the_query', $the_query ); ?>
<?php //get_template_part("template-parts/content", "pagination"); ?>


  <nav aria-label="Page navigation example ">

	<?php my_pagination(); ?>

  </nav>
</section>

<script>
	
</script>
<?php
//get_sidebar();

get_footer();
