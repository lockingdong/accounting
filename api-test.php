<?php
/**
  Template Name: api Page
 */



$tw->title = "中華稅務代理人協會";
$tw->href = get_the_permalink(20);
$tw->content = '<p>'.get_field("right_contents", 20)[0]["content"].'</p>';
$tw->img = get_field("left_main_img", 20)['url'];
$twJSON = json_encode($tw);

$tpc->title = "台北市稅務代理人協會";
$tpc->href = get_the_permalink(306);
$tpc->content = '<p>'.get_field("right_contents", 306)[0]["content"].'</p>';
$tpc->img = get_field("left_main_img", 306)['url'];
$tpcJSON = json_encode($tpc);

$ntpc->title = "新北市稅務代理人協會";
$ntpc->href = get_the_permalink(289);
$ntpc->content = '<p>'.get_field("right_contents", 289)[0]["content"].'</p>';
$ntpc->img = get_field("left_main_img", 289)['url'];
$ntpcJSON = json_encode($ntpc);

$tyh->title = "桃園市稅務代理人協會";
$tyh->href = get_the_permalink(324);
$tyh->content = '<p>'.get_field("right_contents", 324)[0]["content"].'</p>';
$tyh->img = get_field("left_main_img", 324)['url'];
$tyhJSON = json_encode($tyh);

$tcc->title = "台中市稅務代理人協會";
$tcc->href = get_the_permalink(341);
$tcc->content = '<p>'.get_field("right_contents", 341)[0]["content"].'</p>';
$tcc->img = get_field("left_main_img", 341)['url'];
$tccJSON = json_encode($tcc);

$tnh->title = "台南市稅務代理人協會";
$tnh->href = get_the_permalink(354);
$tnh->content = '<p>'.get_field("right_contents", 354)[0]["content"].'</p>';
$tnh->img = get_field("left_main_img", 354)['url'];
$tnhJSON = json_encode($tnh);

$khc->title = "高雄市稅務代理人協會";
$khc->href = get_the_permalink(366);
$khc->content = '<p>'.get_field("right_contents", 366)[0]["content"].'</p>';
$khc->img = get_field("left_main_img", 366)['url'];
$khcJSON = json_encode($khc);

//echo $myJSON;
 ?>

<?php 
//$s =  var_dump(get_field("right_contents", 20)[0]["content"]);
  $json = json_decode(
  '[
    '. $twJSON .',
    '. $tpcJSON .',
    '. $ntpcJSON .',
    '. $tyhJSON .',
    '. $tccJSON .',
    '. $tnhJSON .',
    '. $khcJSON .'
  ]');


  echo json_encode($json);
?>

