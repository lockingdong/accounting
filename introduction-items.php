<?php  
$test = get_field("title", 6);
$json = json_decode(
'[
  {
   "title": "中華稅務代理人協會'. (string)$test  .'",
   "href": "123",
   "content":"<p>《大悲心陀羅尼經》中，佛陀告訴阿難尊者說：「如是神咒，有種種名：一名廣大圓滿、一名無礙大悲、一名救苦陀羅尼、一名延壽陀羅尼、一名滅惡趣陀羅尼、一名破惡業障陀羅尼、一名滿願陀羅尼、一名隨心自在陀羅尼、一名速超十地陀羅尼。」</p><p>此咒名稱彰顯了觀世音菩薩欲安樂利益一切眾生之廣大圓滿、無礙大悲的大悲願力，及此咒不但能除一切災難、以及諸惡業病苦；且能成就一切善法、隨心滿願；遠離一切怖畏、速登佛地的威神特德。今人以大悲咒簡稱之，取之觀世音菩薩慈披眾生之大悲願力而命名。</p>",
   "img": "logo-roc.png",
   "logo": "logo-roc.png",
  },
  {
   "title": "台北市稅務代理人協會",
   "href": "d2",
   "content":"中華稅務代理人協會2",
   "img": "logo-taipai.png",
   "logo": "logo-taipai.png",
  },
  {
   "title": "新北市稅務代理人協會",
   "href": "d3",
   "content":"中華稅務代理人協會3",
   "img": "logo-newtaipai.png",
   "logo": "logo-newtaipai.png",
  },
  {
   "title": "桃園市稅務代理人協會",
   "href": "d4",
   "content":"中華稅務代理人協會4",
   "img": "logo-taoyuan.png",
   "logo": "logo-taoyuan.png"
  },
  {
   "title": "臺中市稅務代理人協會",
   "href": "d5",
   "content":"中華稅務代理人協會5",
   "img": "logo-taic.png",
   "logo": "logo-taic.png"
  },
  {
   "title": "臺南市稅務代理人協會",
   "href": "d6",
   "content":"中華稅務代理人協會6",
   "img": "logo-tainan.png",
   "logo": "logo-tainan.png"
  },
  {
   "title": "高雄市稅務代理人協會",
   "href": "d7",
   "content":"中華稅務代理人協會7",
   "img": "logo-ko.png",
   "logo": "logo-ko.png"
  }
]');


echo json_encode($json);
?>

  
 