<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */
$page_name = "活動花絮";
$middle_pages = json_decode('[

]');
$page_title = "活動花絮";
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

	<!-- <header class="page-header">
		<?php
			//the_archive_title( '<h1 class="page-title">', '</h1>' );
			//the_archive_description( '<div class="archive-description">', '</div>' );
		?>
		<?php 
			//$obj = get_queried_object();
			//$cate_name =  $obj -> {"category_nicename"};
		?>
	</header> -->


	<?php
		// $latest_tags_count = get_field('latest_tags_count');
		// $hot_tags_count = get_field('hot_tags_count');
		$cur = get_query_var('paged');

		$the_query = new WP_Query(array(
			'posts_per_page'=>6,
			'post_type' => 'highlight',
			'paged' => $cur
		));

	?>

<style type="text/css">
	.blooper-items .card-text{
		word-break:break-all;
	}

</style>
	


<section class="container content-wrapper">
  <div class="row">




	<?php while ($the_query -> have_posts()) : 
		$the_query -> the_post(); 
	?>

  
    <div class="col-xl-4  col-lg-6  col-md-6 col-12 blooper-items">
      <div class="card">
        <div class="card-img">
          <a href="<?php echo get_permalink() ;?>">
		  <?php 
			$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
			if(!empty($post_thumbnail_id)) :?>
			<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
			<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
			<img src="<?php echo $img_ar[0];?>"
				alt="<?php echo $img_alt;?>"
			/>
		<?php endif; ?>
          </a> 
        </div>
        <div class="card-body">
          <!-- <h5 class="card-title text-secondary">
		  <?php echo get_the_date( 'Y' )-1911; ?>年
		  <?php echo get_the_date( 'm' ); ?>月
		  <?php echo get_the_date( 'd' ); ?>日</h5> -->
          <p class="card-text"><?php echo wp_trim_words(get_the_title(), 20, '...'); ?></p>
        </div>
      </div>
    </div>
    
	<?php
		endwhile;
		wp_reset_postdata(); 
	?>

  </div>
  <nav aria-label="Page navigation example ">
	<?php my_pagination(); ?>
  </nav>
</section>

		

<?php
//get_sidebar();
get_footer();
