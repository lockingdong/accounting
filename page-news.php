<?php
/**
  Template Name: 協會新聞
 */
$page_name = "協會新聞";
$middle_pages = json_decode('[

]');
$page_title = get_the_title();
get_header(); ?>
<style type="text/css">
	.news-date {
		width: 60px!important;
	}
</style>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

<div style="display: none;">
	<?php echo do_shortcode( '[searchandfilter fields="search,post_date" types=",daterange" ]' ); ?>
</div>

<section class="container-fluid content-wrapper ">
	<article class="container">
		<form novalidate class="search-form" >
		<div>
			<span class="search-classification">日期</span> 
			<input  type="text" name="date1" placeholder="01 /01 / 2018" class="start birth-date search-colum "  id="datepicker">
			<input  type="text" name="date2" placeholder="01 /01 / 2018" class="end birth-date search-colum "  id="datepicker2">
		</div>
		<div>
			<span class="search-classification">關鍵字</span> <input type="text" class="keywords-colum search-colum" placeholder="想要收尋什麼"> 
			<button type="submit" class="btn btn-primary search-btn">查詢</button>
		</div>
		</form>
	</article>


	<hr class="separation-line">

	
		
	


<article class="container content-wrapper news-list-category">  
	

			<?php
				$latest_tags_count = get_field('latest_tags_count');
				$hot_tags_count = get_field('hot_tags_count');
				$cur = get_query_var('paged');

				$the_query = new WP_Query(array(
					'posts_per_page'=>10,
					'post_type' => 'post',
					'paged' => $cur,
					'ignore_sticky_posts' => 1,
				));

				$num = 1;

				$page_count = $the_query->post_count;
				$count = $the_query->found_posts;
				$r_num = 0;
			?>

			<?php while ($the_query -> have_posts()) : 
				$the_query -> the_post(); 
			?>

			
			<?php if($cur!=0): ?>
				<?php 
					//echo $num + ($cur-1) * $the_query->query_vars['posts_per_page']; 
					$r_num = $num + ($cur-1) * $the_query->query_vars['posts_per_page'];
				?>
			<?php else: ?>
				<?php 
					//echo $num; 
					$r_num = $num;
				?>
			<?php endif; ?>

		<div class="row news-info-items">
			<div class="col-md-1 col-2 my-auto px-0">
				<div class="news-date">
				<div class="new-num text-center bg-secondary ">
					<h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
				</div>
				<div>
					<div class="new-mon text-center bg-primary text-nowrap">
					<h6><?php echo get_the_date( 'M' ); ?></h6>
					</div>
				</div>
				</div>
			</div>
			<div class="col-md-11 col-9 px-0 news-info-line">
				<div class="row">
				<div class="col-10 px-0 ">
					<div class="news-list">
					<div class="news-list-title">
						<a href="<?php echo get_permalink() ;?>"
						   post_id="<?php echo get_the_ID() ;?>"
						   class="news-link"
						>
						
						<h5><?php echo wp_trim_words( get_the_title(), 60, '...' ); ?></h5>

						
						</a>
						<?php if( $r_num <= $latest_tags_count):?>
							<a href="<?php echo get_post_permalink(87); ?>">
								<span class="badge badge-new">最新</span>
							</a>

						<?php endif; ?>
						
					</div>
					</div>
				</div>
				<div class="col-2 my-auto news-info-right">
					<a href="<?php echo get_permalink() ;?>" class="">＞</a>
				</div>
				</div>
			</div>
		</div>

			
			

		<?php $num++; ?>
				
		<?php
			endwhile;
			wp_reset_postdata(); 
		?>

			

		<nav aria-label="Page navigation example ">
			<?php my_pagination(); ?>
		</nav>

		
	</article>



	<?php 
		
		$args2 = array(
			'post_type' => 'post',
			'meta_key' => 'views_total',
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
			'posts_per_page' => $hot_tags_count,
			'ignore_sticky_posts' => 1,
		);

		$top_posts = new WP_Query($args2);

		//echo var_dump($top_posts);
	?>

	<div class="hot-post-hint" style="display: none;">
		<?php while ($top_posts -> have_posts()) : 
			$top_posts -> the_post(); 
		?>
			<li>
				<?php echo get_the_ID(); ?>
			</li>

		<?php
			endwhile;
			wp_reset_postdata(); 
		?>
	</div>
</section>


	
<script>
	$(document).ready(function(){

		let hint_array = [];
		$(".hot-post-hint li").each(function(){
			hint_array.push(parseInt($(this).html()));
		});


		$(".news-link").each(function(index){
			var x = $(this).attr("post_id");

			for(i=0;i<hint_array.length;i++){
				if(hint_array[i]==x){
					//console.log(this)
					$(this).parent().append(`
						<a href='<?php echo get_post_permalink(105); ?>'>
							<span class="badge badge-hot">熱門</span>
						<a/>`);
				}
			}


		});
		
	});

	
	$(".search-form .search-btn").click(function(e){
//?s=test2&post_date=2018-09-11+2018-09-12

		e.preventDefault();
		var s = ($(".keywords-colum").val()==="")?"":$(".keywords-colum").val();
		var start = new_date($(".start").val());
		var end = new_date($(".end").val());

		var host = window.location.host;
		var search = "?s="+s+"&post_date="+ start +"+"+ end;
		var newURL = window.location.protocol + "//" + window.location.host + "/"  + search


		window.open(newURL);
	});

	function new_date($date){
		var y = $date.substr(6, 4);
		var m = $date.substr(0, 2);
		var d = $date.substr(3, 2);
		return y+"-"+m+"-"+d;
	}
	setTimeout(function(){
		$(".start").val("01/01/2018");
		//$(".end").val("");
	}, 1000)
	
	
</script>
<?php
get_footer();
