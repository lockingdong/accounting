<?php
/**
 * ken-cens.com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ken-cens.com
 */

if ( ! function_exists( 'ken_cens_com_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ken_cens_com_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ken-cens.com, use a find and replace
	 * to change 'ken-cens-com' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ken-cens-com', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'ken-cens-com' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ken_cens_com_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ken_cens_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ken_cens_com_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ken_cens_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'ken_cens_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ken_cens_com_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ken-cens-com' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ken_cens_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ken_cens_com_scripts() {
	wp_enqueue_style( 'ken-cens-com-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ken-cens-com-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ken-cens-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ken_cens_com_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function dong_create_org_term(){

	register_post_type('org-term',
		array(
			'labels' => array(
				'name' => '組織條例',
				'singular_name' => '組織條例',
			),
			'public' => true,
			'menu_position' => 3,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'rest_base' => 'org-term',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'comments',
				
			),
		)
	);

}

add_action('init', 'dong_create_org_term', 1);

function dong_create_chairman(){

	register_post_type('chairman',
		array(
			'labels' => array(
				'name' => '理事長專區',
				'singular_name' => '理事長專區',
			),
			'public' => true,
			'menu_position' => 4,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'rest_base' => 'chairman',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				'thumbnail',
				
			),
		)
	);

}

add_action('init', 'dong_create_chairman', 1);



function dong_create_register(){

	register_post_type('register',
		array(
			'labels' => array(
				'name' => '申請入會',
				'singular_name' => '申請入會',
			),
			'public' => true,
			'menu_position' => 5,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'has_archive' => true,
			'rest_base' => 'register',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'comments',
				
			),
		)
	);

}
add_action('init', 'dong_create_register', 1);




function dong_create_highlight(){
	register_post_type('highlight',
		array(
			'labels' => array(
				'name' => '活動花絮',
				'singular_name' => '活動花絮',
			),
			'public' => true,
			'menu_position' => 6,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'has_archive' => true,
			'rest_base' => 'highlight',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				//'excerpt',
				'thumbnail',
				//'comments',
				
			),
		)
	);

}

add_action('init', 'dong_create_highlight', 1);







function dong_create_act(){
	register_post_type('act',
		array(
			'labels' => array(
				'name' => '會務活動',
				'singular_name' => '會務活動',
			),
			'public' => true,
			'menu_position' => 7,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'has_archive' => true,
			'rest_base' => 'act',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				//'excerpt',
				'thumbnail',
				'comments',
				
			),
		)
	);

}

add_action('init', 'dong_create_act', 1);
function dong_create_taxonomies(){
	$labels = array(
		'name' => '活動分類',
		'singular_name' => '活動分類',
	);

	register_taxonomy('act-type', array('act'),
		array(
			'has_archive' => true,
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
		)
	);
}
add_action('init', 'dong_create_taxonomies', 1);





// function dong_create_taxonomies(){
// 	$labels = array(
// 		'name' => '顯示於首頁',
// 		'singular_name' => '顯示於首頁',
// 	);

// 	register_taxonomy('location', array('products'),
// 		array(
// 			'hierarchical' => true,
// 			'labels' => $labels,
// 			'show_ui' => true,
// 			'show_admin_column' => true,
// 			'show_in_nav_menus' => true,
// 		)
// 	);

// }
// add_action('init', 'dong_create_taxonomies', 0);



/*
 * Adds the views column to Posts Manage page
 */
function add_wpp_views_columns($columns) {

    if ( function_exists('wpp_get_views') ) {
        $columns['views'] = 'Views';
    }

    return $columns;
}
add_filter('manage_posts_columns', 'add_wpp_views_columns');

/*
 * Displays the views column data
 */
function wpp_views_columns_data($name) {

    if ( 'views' == $name && function_exists('wpp_get_views') ) {
        global $post;
        echo wpp_get_views( $post->ID );
    }

}
add_action('manage_posts_custom_column',  'wpp_views_columns_data');



/* Storing views of different time periods as meta keys */
add_action( 'wpp_post_update_views', 'custom_wpp_update_postviews' );

function custom_wpp_update_postviews($postid) {
	// Accuracy:
	//   10  = 1 in 10 visits will update view count. (Recommended for high traffic sites.)
	//   30 = 30% of visits. (Medium traffic websites)
	//   100 = Every visit. Creates many db write operations every request.

	$accuracy = 50;

	if ( function_exists('wpp_get_views') && (mt_rand(0,100) < $accuracy) ) {
		// Remove or comment out lines that you won't be using!!
		update_post_meta( $postid, 'views_total',   wpp_get_views( $postid )            );
		update_post_meta( $postid, 'views_daily',   wpp_get_views( $postid, 'daily' )   );
		update_post_meta( $postid, 'views_weekly',  wpp_get_views( $postid, 'weekly' )  );
		update_post_meta( $postid, 'views_monthly', wpp_get_views( $postid, 'monthly' ) );
	}
}


function debug_to_console( $data ) {
	if ( is_array( $data ) )
		$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
		else
		$output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
	echo $output;
}



// function my_acf_save_post( $post_id ) {
    
//     //bail early if no ACF data
//     // if( empty($_POST['acf']) ) {
        
//     //     return;
        
//     // }
    
    
//     // // array of field values
// 	// $fields = $_POST['acf'];
// 	// debug_to_console(var_dump($fields));

// 	debug_to_console(123);
//     // specific field value
//     //$field = $_POST['acf']['field_abc123'];
    
// }

// add_action('acf/save_post', 'my_acf_save_post', 1);

// function my_acf_save_post( $post_id ) {
    
//     // get new value
// 	$value = get_field('latest_tags_count');
// 	debug_to_console($value);

	
    
// }

// add_action('acf/save_post', 'my_acf_save_post', 20);

add_filter( 'comment_form_fields', 'example_order_comment_form_fields' );

function example_order_comment_form_fields( $fields ) {

  // Move comment field last.
 $fields['comment'] = array_shift( $fields );

  return $fields;

}



//==============use jquery==============//
function wpdocs_scripts_method() {
    wp_enqueue_script( 'custom-script', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );






class IBenic_Walker extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu dropdown-sub\" aria-labelledby=\"navbarDropdownMenuLink\" >\n";
	}
    
	function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
		$object = $item->object;
    	$type = $item->type;
    	$title = $item->title;
    	$description = $item->description;
		$permalink = $item->url;
		
		if( $permalink && $permalink != '#' ) {
			$output .= "<li class=' " .  implode("nav-item " , $item->classes) . "'>";
		}else{
			if($depth ===1){
				$output .= "<li class='" .  implode("" , $item->classes) . "'>";
			}else{
				$output .= "<li class='" .  implode("nav-item dropdown " , $item->classes) . "'>";			
			}

		}
		
        
		//Add SPAN if no Permalink
		if( $permalink && $permalink != '#' ) {
			$output .= '<a class="nav-link dropdown-item" href="' . $permalink . '"><span class="d-inline-block mobile-item">';
		
		}else if($depth===0){
			$output .= '<a class="dropdown-item nav-link  desk-nav-dropdown" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="d-inline-block mobile-item">' ;
		}else if($depth===1){
			$output .= '<a class="dropdown-item dropdown-main" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="d-inline-block mobile-item">' ;
		}else if($depth===2){
			$output .= '<a class="dropdown-item dropdown-main" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="mobile-item">' ;
		}
		else{
			$output .= '<a class="nav-link  desk-nav-dropdown dropdown-item" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="d-inline-block mobile-item">' ;
		}


		
		$output .= $title;
		if( $description != '' && $depth == 0 ) {
			$output .= '<small class="description">' . $description . '</small>';
		}


		if( $permalink && $permalink != '#' ) {
			$output .= '</a>';
		} else {
			$output .= '<i class="fas fa-angle-down text-right"></i></span>
			</a>';
		}
	
	  
	}
	
}

// add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
// function current_type_nav_class($classes, $item) {
//     // Get post_type for this post
//     $post_type = get_query_var('post_type');

//     // Go to Menus and add a menu class named: {custom-post-type}-menu-item
//     // This adds a 'current_page_parent' class to the parent menu item
//     if( in_array( $post_type.'-menu-item', $classes ) )
//         array_push($classes, 'current_page_parent');

//     return $classes;
// }


function my_pagination() {

	global $the_query;
	
	if ( $the_query->max_num_pages <= 1 ) return; 
	
	$big = 999999999; // need an unlikely integer
	
	$pages = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $the_query->max_num_pages,
		'type'  => 'array',
		'prev_text' => "«",
		'next_text' => "»",
	) );
	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
		echo '<ul class="pagination justify-content-center" style="opacity:0;">';
		foreach ( $pages as $page ) {
				echo "<li class='page-item'>$page</li>";
		}
	   echo '</ul>';
	}
}


function wpdocs_unregister_tags_for_posts() {
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'wpdocs_unregister_tags_for_posts' );


function disable_new_posts() {
// Hide sidebar link
// global $submenu;
//unset($submenu['edit.php?post_type=jxta_home'][10]);

// Hide link on listing page
if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
    echo '<style type="text/css">
    .page-title-action { display:none; }
    </style>';
 }
}
add_action('admin_menu', 'disable_new_posts');


function edit_comment_text() {
	if (isset($_GET['action']) && $_GET['action'] == 'edit') {
	    echo '<style type="text/css">
			.page-title-action { display:none; }
	    </style>';
	}
}
add_action('admin_menu', 'edit_comment_text');

//display: none //page quick edit
function remove_page_quick_edit() {
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
	    echo '<style type="text/css">
			.editinline { display:none; }
	    </style>';
	}
}
add_action('admin_menu', 'remove_page_quick_edit');


// Add the custom columns to the book post type:
add_filter( 'manage_act_posts_columns', 'act_status_columns' );
function act_status_columns($columns) {
    //unset( $columns['author'] );
    $columns['comment_status'] = __( '活動狀態', 'your_text_domain' );
    //$columns['publisher'] = __( 'Publisher', 'your_text_domain' );
	// $newColumns = array();
	// $newColumns['status'] = '活動狀態';
    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_act_posts_custom_column' , 'custom_act_column', 10, 2 );
function custom_act_column( $column, $post_id ) {
	switch ( $column ) {
		case 'comment_status' :
			//echo get_post_meta( $post_id , 'publisher' , true ); 
			echo (!comments_open())?"已結束":"<span style='color:red;'>開放中</span>";
			break;

	}
}


add_filter( 'manage_edit-act_sortable_columns', 'my_website_manage_sortable_columns' );
function my_website_manage_sortable_columns( $sortable_columns ) {

   $sortable_columns[ 'comment_status' ] = "comment_status";
   // Let's also make the film rating column sortable
   //$sortable_columns[ 'film_rating_column' ] = 'film_rating';
   return $sortable_columns;

}

add_filter( 'posts_orderby', function( $orderby, \WP_Query $q ) use ( &$wpdb )
{   
    $_orderby = $q->get( 'orderby' );
    $_order   = $q->get( 'order' );

    if( 
           is_admin() 
        && $q->is_main_query() 
        && did_action( 'load-edit.php' )
        && 'comment_status' === $_orderby 
    )
        $orderby = " {$wpdb->posts}.comment_status " 
            . ( 'ASC' === strtoupper( $_order ) ? 'ASC' : 'DESC' )
            . ", {$wpdb->posts}.ID DESC ";

    return $orderby;
}, 10, 2 );






// remove toolbar items
// https://digwp.com/2016/06/remove-toolbar-items/
function shapeSpace_remove_toolbar_node($wp_admin_bar) {
	
	// replace 'updraft_admin_node' with your node id
	$wp_admin_bar->remove_node('new-content');
	$wp_admin_bar->remove_node('customize');
	
}
add_action('admin_bar_menu', 'shapeSpace_remove_toolbar_node', 999);



// if ( !is_admin() ) {
// 	function remove_screen_options(){ __return_false;}
// 	add_filter('screen_options_show_screen', 'remove_screen_options');
// }

function wpb_remove_screen_options() { 
	if(!current_user_can('manage_options')) {
	return false;
	}
	return true; 
	}
add_filter('screen_options_show_screen', 'wpb_remove_screen_options');


function disable_author_field() {

	echo '<script>
	if(document.getElementById("acf-field-author")){
	document.getElementById("acf-field-author").setAttribute("readonly","true");
	}
	</script>';
}
add_action('admin_footer', 'disable_author_field');



function not_allow_author_sticky(){
	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'administrator');
	if( !array_intersect($allowed_roles, $user->roles ) ) {
		echo "<style>#act-type-31,#popular-act-type-31{display:none;}</style>";
	}
}
add_action('admin_footer', 'not_allow_author_sticky');



add_action('comment_post', 'comment');
function comment($comment_reply_id) 
{
    $comment = get_comment($comment_reply_id);
    if($comment->comment_parent != 0) 
    {
        $old_comment = get_comment($comment->comment_parent);
        if($old_comment->user_id == 0)
        {
            $email = $old_comment->comment_author_email;
            $name = $comment->comment_author;
            $content = $comment->comment_content;
            $post = get_post($comment->comment_post_ID);
            $title = $post->post_title;
            $link = get_permalink($comment->comment_post_ID);
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            $subject = sprintf('Comment reply: "%2$s" at %1$s', $blogname, $title );
            $notify_message  = sprintf('Someone replied to a comment you left on: %s', $title ) . "\r\n";
            $notify_message .= sprintf( 'Reply by: %1$s ', $name ) . "\r\n";
            $notify_message .= 'Comment: ' . "\r\n" . $content . "\r\n\r\n";
            $notify_message .= 'You can reply to the comment here: ' . "\r\n";
            $notify_message .= $link . "#comments\r\n\r\n";
            $message_headers = "Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n";
            wp_mail( $email, $subject, $notify_message, $message_headers );
        }
    } 
}




function tablepress1() {
	$current_user = wp_get_current_user();
	if($_GET['page']=='tablepress'&&$_GET['action']!='edit'){
		echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>';
		echo '<script>';
			echo '$("#the-list tr").css("display", "none");';
			echo 'var user_id = '.$current_user->ID.';';
			echo 'console.log(user_id);';
			echo '$(".column-table_id").each(function(){
				if($(this).html()==user_id){
					$(this).parent().attr("style","");
				}
			});';
		echo '</script>';
	}
}
add_action('admin_footer', 'tablepress1');
	

function tablepress2() {
	$current_user = wp_get_current_user();
	if($_GET['page']=='tablepress_import'){
		echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>';
		echo '<script>';
			echo '$("#tables-import-type-add").parent().css("display", "none");';
			echo '$("#tables-import-type-replace").attr("checked","true");';
			echo 'var user_id = '.$current_user->ID.';';
			echo '$("#tables-import-existing-table option[value='.$current_user->ID.']").attr("selected","true");';
			echo 'setTimeout(function(){
						$("#tables-import-existing-table").attr("disabled","true");
						$("#tables-import-existing-table").attr("style","pointer-events:none;");
					},500);';
		echo '</script>';
	}
}
add_action('admin_footer', 'tablepress2');


function tablepress_edit() {
	$current_user = wp_get_current_user();
	if($_GET['page']=='tablepress'&&$_GET['action']=='edit'){
		echo '<style>
		#tablepress_edit-table-information,#tablepress_edit-table-options,#tablepress_edit-datatables-features
		{display:none;}</style>';
	}
}
add_action('admin_footer', 'tablepress_edit');
//$("#tables-import-type-replace").attr("checked","true");


function update_test( $post_id, $post ) { 
	if (isset($post->post_status) && 'auto-draft' == $post->post_status) {
	  return;
	}
	update_post_meta($post_id, 'copied', '1');
	update_post_meta($post_id, 'blurb', 'this value updated by save_post action');
	
  }
  add_action( 'save_post', 'update_test', 1, 2);





//分類必填
function custom_rpc_post_types( $post_types ) {
    $post_types['act'] = array(
        'act-type' => array(
            'message' => "請至少選擇一個分類"
        )
    );
    return $post_types;
}
add_filter( 'rpc_post_types', 'custom_rpc_post_types' );


function custom_rpc_post( $post_types ) {
    $post_types['post'] = array(
        'category' => array(
            'message' => "請至少選擇一個分類"
        )
    );
    return $post_types;
}
add_filter( 'rpc_post_types', 'custom_rpc_post' );



//更改“回響”
function change_text() {

	echo '
	<style>
	#category-tabs,#act-type-tabs{display:none;}
	</style>
	<script>
	if(jQuery("#commentstatusdiv")){
	jQuery("#commentstatusdiv").html(jQuery("#commentstatusdiv").html().replace(/迴響/g, "報名"));
	}
	</script>';
}
add_action('admin_footer', 'change_text');



//hidden comment some action
function hide_comment_action(){
	echo '
		<style>
			#the-comment-list .edit, #the-comment-list .quickedit {display:none;}
		</style>
	';
	
}

add_action('admin_footer', 'hide_comment_action');


//csv_download
function csv_download() {

	if (isset($_GET['action']) && $_GET['action'] == 'edit') {

		$post_id = $_GET['post'];
		echo 
		'<script>
			jQuery(".csv-btn").css("display","block");
			//jQuery(".csv-btn").html();
			console.log(document.location.origin);
			jQuery(".csv-btn").attr("href", document.location.origin+"/csv/?id='.$post_id.'");
		
		</script>';
	}
}
add_action('admin_footer', 'csv_download');


flush_rewrite_rules(); 