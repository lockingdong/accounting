<?php
/**
  Template Name: 相關連結
 */

$page_name = "相關連結";
$middle_pages = json_decode('[

]');
$page_title = "相關連結";
get_header(); ?>

<style>
	.paginate-list.row {
		padding: 0;
		background: #f6f6f6;
		margin-bottom: 50px;
	}
	.paginate-list.row li {
		list-style: none;
		padding: 10px 15px;
		/* background: rgba(238,238,238,.5); */
	}

	@media (max-width: 576px){
		.paginate-list.row li:nth-of-type(odd) {
			background-color:#f6f6f6;
		}
	}
	@media (min-width: 576px){
		.paginate-list.row li:nth-of-type(4n+3), .paginate-list.row li:nth-of-type(4n+3) + * {
			background-color:white;
		}
		.paginate-list.row li:nth-of-type(4n+1), .paginate-list.row li:nth-of-type(4n+1) + * {
			background-color:#f6f6f6;
		}
	}


	.pagination li {
		list-style: none;
	}
	.pagination li.active a {
		background-color: #ddf5fb;
		border-color: #ddf5fb;
	}
	

	.pagination li a {
		padding: .5rem .75rem;
		border: 1px solid #dee2e6;
		border-left: none;
		cursor: pointer;
		color: #5894f4!important;
	}
	.pagination li a:hover {
		background-color: #e9ecef;
	}

	.pagination li:nth-child(1) a {
		padding: .5rem .75rem;
		border: 1px solid #dee2e6;
		border-radius: .25rem 0 0 .25rem;
	}
	.pagination li:last-child a {
		
		border-radius: 0 .25rem .25rem 0;
	}
	.pagination li.active a:hover{
		background: #ddf5fb !important;
		color: #5894f4!important;
	}

	#related-link-page .row {
		background: white !important;
	}
	.paginate-list a {
		text-decoration: none;
		transition: .3s;
	}
	.paginate-list a:hover {
		color: #c65a3e;
	}
</style>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

	

<?php 
	$objs = get_field("all_links");
?>

<section class="container content-wrapper" id="related-link-page">
  <h3 class="content-title">相關連結</h3>
  <span class="star-symbol">★★★★★</span>

<div id="app">
  <paginate :per="<?php echo get_field("per_page_link"); ?>" name="items" :list="items" class="paginate-list row">
    <li class="col-sm-6 col-12  text-left" v-for="item in paginated('items')">
		<a :href="item.link_url" target="_blank">
		<span class="arrow-icon bg-primary"><i class="fas fa-angle-right text-white"></i></span>
			{{ item.link_name }} 
		</a>
    </li>
  </paginate>

	<nav aria-label="Page navigation example">
  	<paginate-links class="pagination justify-content-center" for="items" :limit="3" :show-step-links="true"></paginate-links>
	</nav>
</div>


</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://rawgit.com/TahaSh/vue-paginate/master/dist/vue-paginate.js"></script>
<script>
	new Vue({
	el: '#app',
	data: {
		items: <?php echo json_encode($objs); ?>,
		paginate: ['items']
	}
})

</script>

<?php
//get_sidebar();
get_footer();
