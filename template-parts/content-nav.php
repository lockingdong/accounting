<div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav mx-auto">
    <li class="nav-item dropdown">


        <a class="nav-link  desk-nav-dropdown" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="d-inline-block mobile-item">
            本會簡介 <i class="fas fa-angle-down"></i>
        </span>
        </a>

        
        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">中華稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a> 
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(20);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(23);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(57);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(33);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(64);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">台北市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(306);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(311);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(316);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(319);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(308);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">新北市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(289);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(297);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(300);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(302);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(293);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">桃園市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(324);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(329);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(331);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(335);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(327);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">臺中市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(341);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(345);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(347);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(349);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(343);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">臺南市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(354);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(358);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(360);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(362);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(356);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        <li><a class="dropdown-item dropdown-main" href="#"><span class="d-inline-block text-left">高雄市稅務代理人協會</span><i class="fas fa-angle-down text-right"></i></a>
            <ul class="dropdown-menu dropdown-sub">
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(366);?>"> <span class="d-inline-block text-left w-15 m-auto">成立宗旨</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(370);?>"> <span class="d-inline-block text-left w-15 m-auto">理監事簡介</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(372);?>"> <span class="d-inline-block text-left w-15 m-auto">組織圖</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(374);?>"> <span class="d-inline-block text-left w-15 m-auto">組織條例</span></a></li>
            <li><a class="dropdown-item" href="<?php echo get_the_permalink(368);?>"> <span class="d-inline-block text-left w-15 m-auto">業務簡介</span></a></li>
            </ul>
        </li>
        </ul> 
    </li>
    <li class="nav-item">
        <span class="d-inline-block mobile-item">
        <a class="nav-link " href="<?php echo get_the_permalink(72);?>">協會新聞</a>
        </span>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link  desk-nav-dropdown" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="d-inline-block mobile-item">
            會務活動 <i class="fas fa-angle-down"></i>
            </span> 
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <li><a class="dropdown-item dropdown-main" href="<?php echo get_the_permalink(187);?>">會務活動</a></li>
        <li><a class="dropdown-item dropdown-main" href="<?php echo get_post_type_archive_link( 'highlight' ); ?>">活動花絮</a></li>
        </ul>
    </li>
    <li class="nav-item">
        <span class="d-inline-block mobile-item">
        <a class="nav-link " href="<?php echo get_the_permalink(147);?>">會員名單</a>
        </span>
    </li>
    <li class="nav-item">
        <span class="d-inline-block mobile-item">
        <a class="nav-link " href="<?php echo get_the_permalink(139);?>">相關連結</a>
        </span>
    </li>     
    </ul>
</div>