<section class="container-fluid header-breadcrumb">
  <div class="container ">
    <h2><?php echo $page_name; ?></h2>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb p-0">
        <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>">首頁</a></li>

<?php //echo json_decode($middle_pages); ?>
				<?php foreach($middle_pages as $key => $page): ?>
					<li class="breadcrumb-item">
						<a href="<?php echo $page->{'url'}; ?>">
							<?php echo $page->{'name'}; ?>
						</a>
					</li>
				<?php endforeach; ?>


				<li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
      </ol>
    </nav>
  </div>
</section>