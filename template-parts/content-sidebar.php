<div class="col-md-4 col-12 order-md-1 order-2">
      <div class="nav-classification">
        <h5 class="nav-title">活動報名</h5>




<!-- ˇˇˇˇˇˇˇˇˇcategoryˇˇˇˇˇˇˇˇˇ -->
	<ul class="px-0 event-registration">
		<li>
			<a href="<?php echo get_post_type_archive_link( 'act' ); ?>">> 所有協會</a>
		</li>
	<?php
		$terms = get_terms('act-type');
 
		foreach ( $terms as $term ) {
 
		// The $term is an object, so we don't need to specify the $taxonomy.
		//echo get_term();

		//echo $term->slug;
		$term_link = get_term_link( $term );
    
		// If there was an error, continue to the next term.
		if ( is_wp_error( $term_link ) || $term->slug=='on_home') {
			continue;
		}
		?>
    	<li>
			<a href="<?php echo $term_link; ?>">
				> <?php echo $term->name; ?>
			</a>
		</li>
		<?php
		}
	?>
	</ul>
<!-- ＾＾＾＾＾category＾＾＾＾＾ -->

      </div>
      <div class="nav-classification">
        <h5 class="nav-title">推薦活動</h5>

<!-- ˇˇˇˇˇˇˇˇˇrelated postˇˇˇˇˇˇˇˇˇ -->

	<?php 
		
		//echo "<pre>", var_dump(get_field("related_act", 187)), "</pre>"
		if( $related_posts ): ?>
		<ul class="px-0 recommended-activity">
		<?php foreach( $related_posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>

			<li>
				<a href="<?php the_permalink(); ?>">
				<div class="row">
					<div class="col-xl-2 col-lg-3 col-md-4 col-2 my-auto  ">
					<?php 
						$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
						if(!empty($post_thumbnail_id)) :?>
						<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
						<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
						<!-- <img src="<?php //echo $img_ar[0];?>"
							alt="<?php //echo $img_alt;?>"
							style="width:65px;height:65px;"
						/> -->
						<div style="
									width:65px;
									height:65px;
									background-image:url(<?php echo $img_ar[0];?>);
									background-size:cover;
									background-position: center center;
									">
						</div>
					<?php endif; ?>
					</div>
					<div class="col-xl-10 col-lg-9 col-md-8 col-10 nav-item-wrapper " > 
					<span class="nav-item-content">
						<?php //the_title(); ?>
						<?php echo wp_trim_words(get_the_title(), 20, '...'); ?>
					</span>
					<p class="nav-news-timestamp">
						<?php echo get_the_date( 'Y' )-1911; ?>年
						<?php echo get_the_date( 'm' ); ?>月
						<?php echo get_the_date( 'd' ); ?>日
						<!-- <br> -->
						<!-- 滿額/<?php //echo get_field("total") ?>人 -->
					</p>
					</div>
				</div>
				</a>
			</li>


		<?php endforeach; ?>
		</ul>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
<!-- ＾＾＾＾＾related post＾＾＾＾＾ -->

      </div>
    </div>