<?php
/**
  Template Name: 本會簡介-業務簡介
 */
global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>




<section class="container content-wrapper ourbusiness">
  <h3 class="content-title"><?php echo get_field('bus_title'); ?></h3>
  <span class="star-symbol">★★★★★</span>
  <?php echo get_field('bus_content'); ?>
  <!-- <p>一、加強國際會計師團體之聯繫，維護國際形象，提昇國際地位。二、汲取國際會計、審計學術新知轉介會員。三、協助政府推展國民外交及國際學術文化交流。四、推展海峽兩岸交流事宜。、加強國際會計師團體之聯繫，維護國際形象，提昇國際地位。二、汲取國際會計、審計學術新知轉介會員。三、協助政府推展國民外交及國際學術文化交流。四、推展海峽兩岸交流事宜。</p>
  <br>
  <br>
  <p>一、紀律委員會</p>
  <p>(一) 人員</p>
  <ul>
    <li>主任委員</li>
    <li>主任委員</li>
    <li>主任委員</li>
  </ul>
  <p>(二) 任務：審議下列事項</p>
  <ul>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
  </ul>
  <p>一、紀律委員會</p>
  <p>(一) 人員</p>
  <ul>
    <li>主任委員</li>
    <li>主任委員</li>
    <li>主任委員</li>
  </ul>
  <p>(二) 任務：審議下列事項</p>
  <ul>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
  </ul>
  <p>一、紀律委員會</p>
  <p>(一) 人員</p>
  <ul>
    <li>主任委員</li>
    <li>主任委員</li>
    <li>主任委員</li>
  </ul>
  <p>(二) 任務：審議下列事項</p>
  <ul>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
  </ul>
  <p>一、紀律委員會</p>
  <p>(一) 人員</p>
  <ul>
    <li>主任委員</li>
    <li>主任委員</li>
    <li>主任委員</li>
  </ul>
  <p>(二) 任務：審議下列事項</p>
  <ul>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
    <li>我是任務我是任務我是任務</li>
  </ul> -->
</section>
	

<?php
//get_sidebar();
get_footer();
