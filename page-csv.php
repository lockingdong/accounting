<?php
/**
  Template Name: csv
 */

// header('Content-Encoding: UTF-8');
// header('Content-type: text/csv; charset=UTF-8');
// header('Content-Disposition: attachment; filename=Customers_Export.csv');
// echo "\xEF\xBB\xBF"; // UTF-8 BOM
// header("Content-type:application/vnd.ms-excel"); 
// header("Content-Disposition:attachment;filename=export_data.xls");

?>

<?php 


$id =  $_GET['id'];



//global $wpdb;

//$results = $wpdb->query( "SELECT * FROM wp_posts" );

$results = $wpdb->get_results( "SELECT * FROM wp_comments WHERE comment_post_ID = $id");

//echo var_dump($results);

//echo "姓名,事務所名稱,聯絡人電話,聯絡人名稱,信箱,是否為會員,備註"."<br>";
$array = [["姓名","事務所名稱","聯絡人電話","聯絡人名稱","信箱","是否為會員","備註"]];
foreach($results as $result){
    $origin_content =  $result->comment_content;
    $newcontent = substr($origin_content, strpos($origin_content, "csv_code:") + 9); 
    $csv_format = explode(',',$newcontent);
    // echo var_dump($newcontent);
    // echo var_dump($csv_format);
    // unset($newcontent);
    // unset($origin_content);
    array_push($array, $csv_format);
}

//echo var_dump($array);


/*
//===========================================



// 2nd Method - Utilizing the $GLOBALS superglobal. Does not require global keyword ( but may not be best practice )

//$results = $GLOBALS['wpdb']->get_results( "SELECT * FROM {$wpdb->prefix}options WHERE option_id = 1", OBJECT );



// $array = ["aaa", "bbb", "ccc", "ddd"];

// function array2csv(array &$array)
// {
//    if (count($array) == 0) {
//      return null;
//    }
//    ob_start();
//    $df = fopen("php://output", 'w');
//    fputcsv($df, array_keys(reset($array)));
//    foreach ($array as $row) {
//       fputcsv($df, $row);
//    }
//    fclose($df);
//    return ob_get_clean();
// }

// function download_send_headers($filename) {
//     // disable caching
//     $now = gmdate("D, d M Y H:i:s");
//     header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
//     header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
//     header("Last-Modified: {$now} GMT");

//     // force download  
//     header("Content-Type: application/force-download");
//     header("Content-Type: application/octet-stream");
//     header("Content-Type: application/download");

//     // disposition / encoding on response body
//     header("Content-Disposition: attachment;filename={$filename}");
//     header("Content-Transfer-Encoding: binary");
// }

// download_send_headers("data_export_" . date("Y-m-d") . ".csv");
// echo array2csv($array);
// die();



//===================================//
$f = fopen("tmp.csv", "w");
foreach ($array as $line) {
    fputcsv($f, $line);
}

header('Content-Disposition: attachment; filename="filename.csv";');
function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    // open raw memory as file so no temp files needed, you might run out of memory though
    $f = fopen('php://memory', 'w'); 
    // loop over the input array
    foreach ($array as $line) { 
        // generate csv lines from the inner arrays
        fputcsv($f, $line, $delimiter); 
    }
    // reset the file pointer to the start of the file
    fseek($f, 0);
    // tell the browser it's going to be a csv file
    header('Content-Type: application/csv');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    // make php send the generated csv lines to the browser
    fpassthru($f);
}



//$array=[[$id,"bb","cc"],["11","22","33"]];

array_to_csv_download($array, // this array is going to be the second row
    "總報名資訊.csv"
);

*/

?>

<?php

header('Content-type:application/vnd.ms-excel');  //宣告網頁格式
    header('Content-Disposition: attachment; filename=總報名資訊.xls');  //設定檔案名稱
if ($_GET['action'] == 'excel') {
    header('Content-type:application/vnd.ms-excel');  //宣告網頁格式
    header('Content-Disposition: attachment; filename=總報名資訊.xls');  //設定檔案名稱
}
else {
    $html_button = '<form action=""><input type="submit" name="action" value="excel"></form>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?=$html_button?>
        <table border="1">
            <!-- <tr>
                <th>a欄</th>
                <th>b欄</th>
                <th>c欄</th>
            </tr> -->

            <?php foreach($array as $item): ?>
            <tr>
                <td><?php echo $item[0];?></td>
                <td><?php echo $item[1];?></td>
                <td><?php echo $item[2];?></td>
                <td><?php echo $item[3];?></td>
                <td><?php echo $item[4];?></td>
                <td><?php echo $item[5];?></td>
                <td><?php echo $item[6];?></td>
                
            </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>

