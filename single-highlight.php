<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */
$page_name = "活動花絮";
$middle_pages = json_decode('[
	{
		"name": "活動花絮",
		"url": "'. get_post_type_archive_link('highlight') .'"
	}

]');
$page_title = get_the_title();
get_header(); ?>



<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

<section class="container content-wrapper blooper-content">
	<?php 
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		if(!empty($post_thumbnail_id)) :?>
		<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
		<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
		<img src="<?php echo $img_ar[0];?>"
			alt="<?php echo $img_alt;?>"
		/>
	<?php endif; ?>
  <h5 class="blooper-content"><?php the_title(); ?></h5>
  <p class="blooper-timestamep">
  <?php echo get_the_date( 'Y' )-1911; ?>年
  <?php echo get_the_date("M"); ?>
  <?php echo get_the_date("d"); ?>日
  </p>


  <?php the_content(); ?>
  
  
  
  
</section>

	<?php endwhile; ?>
<?php endif; ?>








<?php
//get_sidebar();
get_footer();
