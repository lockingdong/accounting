<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */
$page_name = "會務活動";
$middle_pages = json_decode('[
	{
		"name": "會務活動",
		"url": "'. get_post_type_archive_link('act') .'"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>
<?php //echo is_single(); ?>

<style>
	.comment-notes {
		display: none;
	}
	.comment.btn.btn-primary input {
		padding: 0;
		color: white;
		border: none;
		box-shadow: none;
		margin: 0;
		display: inline-block;
		width: 100%;
		height: 100%;
		padding: 8px 12px;
		cursor: pointer;

	}
	.comment.btn.btn-primary .fas {
		transform: translateX(-200%);
		
	}
	.comment.btn.btn-primary:hover * {
		color: black;
		
	}
	.comment.btn.btn-primary {
		padding: 0 !important;
	}

	.input-wrapper {
		/* position: relative; */
	}

	label.error {
		position: absolute;
		width: 100%;
		
		bottom: -10px;
		font-size: 12px;
		color: #f24;
		right: 0;
		/* top: -10px; */
		padding-left: 20px;
	}

	.border {
		border: 2px solid #f24 !important;

	}

	.end {
		position: relative;
	}
	.end:before{
		letter-spacing: 3px;
		font-size: 22px;
		display: flex;
		color: white;
		font-weight: 300;
		content: "活動截止";
		position: absolute;
		justify-content: center;
		align-items: center;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		background: rgba(13,30,76, 0.5);
		z-index: 100;
		pointer-events: none;

	}
	
</style>

<section class="container content-wrapper activities-content">
  <div class="row">
    
<!-- sidebar -->
	<?php 
		$related_posts = get_field("related_act");
	?>
	<?php set_query_var( 'related_posts', $related_posts ); ?>
	<?php get_template_part("template-parts/content", "sidebar"); ?>
<!-- sidebar -->






		<?php //echo get_field("total"); ?>
		<?php //echo get_field("any_info"); ?>
		<?php //echo get_field("any_info2"); ?>
		<?php //echo get_field("act_date"); ?>
		<?php //echo get_field("act_time"); ?>
		<?php //echo get_field("member_price"); ?>
		<?php //echo get_field("other_price"); ?>



		
		<!-- <p><?php //the_content(); ?></p>
		<?php //echo get_the_date("M"); ?>
		<?php //echo get_the_date("d"); ?> -->

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>


	

    <div class="col-md-8 col-12 order-md-2 order-1 ">
      <h3 class="page-content-title"><?php the_title(); ?></h3>
      <div class="activity-img <?php echo (!comments_open())?"end":"";?>">
	  	<?php 
			$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
			if(!empty($post_thumbnail_id)) :?>
			<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
			<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
			<img src="<?php echo $img_ar[0];?>"
				alt="<?php echo $img_alt;?>"
			/>
		<?php endif; ?>
        <div class="activity-img-info">
          <div class="img-info-l">
            <span><?php echo get_field("act_date"); ?></span>
            <span><?php echo get_field("act_time"); ?></span>
          </div>
          <div class="img-info-r">
            <span class="member-price"><?php echo get_field("member_price"); ?></span>
            <span><?php echo get_field("other_price"); ?></span>
          </div>
        </div>
      </div>
      <?php the_content(); ?>

      
      <?php if( get_field('attach') ): ?>
		<a class="filedown" href="<?php echo get_field('attach')['url']; ?>">
		<i class="fa fa-paperclip" aria-hidden="true"></i>
		附件：
			<?php echo get_field('attach')['title']; ?>
		</a>
	<?php endif; ?>
	  
	  <article class="online-registration">


<?php 

$args = array(
	'fields' => apply_filters(
        'comment_form_default_fields', array(
			
			'author' =>
			'<div class="col-md-6 input-wrapper">'.
			'<input id="author" name="author" type="text" value="' .
				esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' required/>'.
				'<span class="placeholder" data-placeholder="姓名"></span>'.'</div>',
				

			'company' => 
			'<div class="col-md-6 input-wrapper">'.
			'<input id="company" name="company" type="text" value="' .
				'' . '" size="30"' . $aria_req . ' required/>'.
				'<span class="placeholder" data-placeholder="事務所名稱"></span>'.'</div>'
			,
			
			'phone' => '<div class="col-md-6 input-wrapper">'.
			'<input id="phone" name="phone" type="text" value="' .
				'' . '" size="30"' . $aria_req . 'required />'.
				'<span class="placeholder" data-placeholder="聯絡人電話"></span>'.'</div>'
			,
			
			'ct-person'  => '<div class="col-md-6 input-wrapper">'.
			'<input id="ct-person" placeholder="聯絡人名稱" name="ct-person" type="text" value="' .
				'' . '" size="30"' . $aria_req . ' />'.'</div>'
			,
			
			'email'  => '<div class="col-md-6 input-wrapper">'.
			'<input id="email" name="email" type="text" value="' .
				'' . '" size="30"' . $aria_req . 'required />'.
				'<span class="placeholder" data-placeholder="信箱"></span>'.'</div>'
			,

			// 'ismember'  => '<div class="col-md-6 input-wrapper">'.
			// '<input id="ismember" name="ismember" type="text" value="' .
			// 	'' . '" size="30"' . $aria_req . 'required />'.
			// 	'<span class="placeholder" data-placeholder="member"></span>'.'</div>'
			// ,

			'ismember'  => '<div class="col-md-6 input-wrapper">'.
			'	
			<select id="ismember" name="ismember">
				<option disabled selected>是否為會員</option>
				<option value="是">是</option>
				<option value="否">否</option>
			</select>
			'
			.'</div>'
			,
            
        )
    ),
    'comment_field' => '<div class="comment-form-comment col-12">' .
        '<label for="comment"></label>' .
        '<textarea id="comment" name="comment" placeholder="備註" cols="45" rows="8" aria-required="true"></textarea>' .
		'</div>',
	'label_submit' =>  __( '送出  ' ),
	'title_reply' => '<div class="crunchify-text"> <h4>線上報名</h4></div>',
	'class_form' => 'row'

);


comment_form($args); 
?>





      </article>
    </div>
	<?php endwhile; ?>
<?php endif; ?>
  </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script>



    $("#commentform").validate({
        rules: {
            "author": { //name屬性
                required: true,

            },
            "sex": { 
                required: true,
            },
            "phone": { //phone
                required: true,
                number: true,
                digits: true,
                minlength: 7,
                maxlength: 10 
                //email: true
            },
            "email": { //email
                required: true,
                email: true,


            },
			"ismember": {
				required: true,
			},
            "address": { //checkbox1
                required: true
            },
            "birth": { //checkbox2
                required: true
            },
            "car": { //科系
                required: false
            },
            "password": { //科系
                required: true,
                minlength : 8,
            
            },
            "password-again": { //科系
                minlength : 8,
                equalTo : "#password"
            }
        },
        messages: {
            username: {
                required: "此為必填欄位",
                minlength: "UserName 至少需要 {0} 個字"
            },
			"author": "請輸入姓名",
			"company": "請輸入事務所名稱",
            "client-name": "請輸入姓名",
            "sex": "請選擇性別",
            "phone": "請輸入有效手機號碼",
            "email": "請輸入有效的信箱",
            "address": "請輸入地址",
            "birth": "請填入生日",
            "password": "請輸入8位字元",
            "password-again": "請輸入相同密碼",
			"ismember": "請選擇是否為會員"
            
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "entry.555108400") {
              error.insertAfter($('.checkbox1-error'));
                //error.insertAfter()
            }else if(element.attr("name") == "entry.367696876"){
              error.insertAfter($('.checkbox2-error'));
            }
            // 
            else{
              error.appendTo(element.parent());
			  element.addClass("border");
            }
        },
        event: "keyup",
        submitHandler: function(form) {

			// $("#comment").val("");
			// $(".form-submit #submit").on('click', function(){

				
			// });


			
			var author = $("#author").val();
			var company = $("#company").val();
			var phone = $("#phone").val();
			var ct_person = $("#ct-person").val();
			var email = $("#email").val();
			var ismember = $("#ismember").val();

			//alert();
			var comment = $("#comment").val();
			$("#comment").val(`
				
				事務所名稱：${company}\n
				聯絡人電話：${phone}\n
				聯絡人名稱：${ct_person}\n
				是否為會員：${ismember}\n
				備註：\n${comment}\n
				
				csv_code:${author},${company},${phone},${ct_person},${email},${ismember},${comment}
			`);
		  

		  alert("報名資訊已寄送！！");
          form.submit();

		  
        }
    });

	

	$("p.form-submit").addClass("col-sm-3 col-5 ml-auto");

	$("input.submit").wrap( "<button class='comment btn btn-primary w-100'></button>" )
	$(".comment.btn.btn-primary").append("<i class='fas fa-arrow-right'></i>");

</script>
<?php
//get_sidebar();
get_footer();
