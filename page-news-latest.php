<?php
/**
  Template Name: 協會新聞-最新消息集結頁
 */
$page_name = "協會新聞";
$middle_pages = json_decode('[

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<section class="container-fluid content-wrapper ">		
	<article class="container content-wrapper news-list-category">  
		

		<?php

			$per_page = get_field('latest_per_page');
			$cur = get_query_var('paged');

			$the_query = new WP_Query(array(
				'posts_per_page'=> $per_page,
				'post_type' => 'post',
				'paged' => $cur,
				'ignore_sticky_posts' => 1
			));

			
		?>

		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>

		
		


	<div class="row news-info-items">
		<div class="col-md-1 col-2 my-auto px-0">
			<div class="news-date">
			<div class="new-num text-center bg-secondary ">
				<h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
			</div>
			<div>
				<div class="new-mon text-center bg-primary text-nowrap">
				<h6><?php echo get_the_date( 'M' ); ?></h6>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-11 col-9 px-0 news-info-line">
			<div class="row">
			<div class="col-10 px-0 ">
				<div class="news-list">
				<div class="news-list-title">
					<a href="<?php echo get_permalink() ;?>"
					post_id="<?php echo get_the_ID() ;?>"
					class="news-link"
					>
					
					<h5><?php echo wp_trim_words( get_the_content(), 60, '...' ); ?></h5>

					
					</a>
					
					
				</div>
				</div>
			</div>
			<div class="col-2 my-auto news-info-right">
				<a href="<?php echo get_permalink() ;?>" class="">＞</a>
			</div>
			</div>
		</div>
	</div>

		

			
	<?php
		endwhile;
		wp_reset_postdata(); 
	?>

		

		<!-- <nav aria-label="Page navigation example ">
			<?php //my_pagination(); ?>
		</nav> -->


	</article>



</section>
		

<?php
//?s=&post_date=2012-01-01+2018-09-13



//get_sidebar();

get_footer();
