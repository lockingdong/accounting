<?php
/**
  Template Name: 本會簡介-理監事簡介
 */
global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<section class="container content-wrapper supervisor">
  <h3 class="content-title"><?php echo get_field('list_main_title'); ?></h3>
  <span class="star-symbol">★★★★★</span>
  <div class="supervisor-list" v-for="(item,idx) in visorlist" >
    <h4 data-toggle="collapse" :data-target="'#list'+idx" @click="inverse()">
      {{item.title}}
        <i class="fas fa-angle-down"></i>
    </h4>
    <div class="collapse supervisor-list-content" :id="'list'+idx">
      <ul>
        <li v-for="inner in item.content_info"><span class="position" >{{inner.position}}</span><span>{{inner.content}}</span></li>
      </ul>
    </div>
  </div>
</section>



<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js"></script>
<script>

var supervisor_list = new Vue({
  el: ".supervisor",
  data: {
    visorlist: <?php echo json_encode(get_field('all_supervisor')) ;?>,
    state: false
  }
});



</script>
<?php
//get_sidebar();
get_footer();
