$(document).ready(function () {
  if ($(window).width() > 992) {
    $(".dropdown").on("click", function (e) {
      e.stopPropagation();
      return ' ';
    });
    $('.dropdown-menu li a.dropdown-item').on('mouseenter', function (e) {
      var $el = $(this);
      var $parent = $(this).offsetParent(".dropdown-menu");
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      var $subMenu = $(this).next(".dropdown-menu");
      $subMenu.toggleClass('show');

      $(this).parent("li").toggleClass('show');

      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-menu .show').removeClass("show");
      });

      if (!$parent.parent().hasClass('navbar-nav')) {
        $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 1 });
      }
      return false;
    });
  } else {
    $('.dropdown-menu a.dropdown-item').on('click', function (e) {
      e.stopPropagation();
      var $el = $(this);
      var $parent = $(this).offsetParent(".dropdown-menu");
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      var $subMenu = $(this).next(".dropdown-menu");
      $subMenu.toggleClass('show');

      $(this).parent("li").toggleClass('show');

      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-menu .show').removeClass("show");
      });

      if (!$parent.parent().hasClass('navbar-nav')) {
        $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() });
      }
    });
  }
  var $h = $("header").height();
  $(".navbar-top").height($h);
  var $hinfo = $(".navbar-info").height();
  $(window).on("scroll", function () {
    var $wstop = $(window).scrollTop();
    if ($wstop > 120) {
      $(".navbar-info").css("height", "0");
      $(".navbar-info p").css("color","#fff");
      $(".navbar-info .fas").css("color","#fff");
      $(".gotop").css("opacity", ".5");
      $(".navbar-bg").css({
        "box-shadow": "0px 5px 8px #eee"
      });
    } else {
      $(".navbar-info").css("height", $hinfo);
      $(".navbar-info p").css("color","#000");
      $(".navbar-info .fas").css("color","#1f3c88");
      $(".gotop").css("opacity", "0");
      $(".navbar-bg").css({
        "box-shadow": "0px 0px 0px transparent"
      });
    }
  });

  //date format yyyy/mm/
  $("#datepicker").datepicker({
    autoclose: true,
    todayHighlight: true
  }).datepicker('update', new Date());
  $("#datepicker2").datepicker({
    autoclose: true,
    todayHighlight: true
  }).datepicker('update', new Date());

  //字數限制
  var $len = 25; // 超過20個字以"..."取代
  $(".piece-writing .col-6 p").each(function () {
    if ($(this).text().length > $len) {
      var $text = $(this).text().substring(0, $len - 1) + "...";
      $(this).text($text);
    }
  });
  var $nav_limit = 50;
  $(".nav-classification .nav-item-content").each(function () {
    if ($(this).text().length > $nav_limit) {
      var $text = $(this).text().substring(0, $nav_limit - 1) + "...";
      $(this).text($text);
    }
  });
  
});

//hambuger
$('.special-button').click(function () {

  if ($('.bottom').hasClass('active')) {
    $(".st0").attr("class", "st0");
  } else {
    $(".st0").attr("class", "st0 active");
  }
  $('.top').toggleClass("active");
  $('.bottom').toggleClass("active");
});

//gotop
$(".gotop").on('click', function () {
  $("html,body").animate({
    scrollTop: 0
  }, 700);
  return false;
});

// index-banner-kv slick
$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav',
  speed: 900,
  cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
  touchThreshold: 100
});
$('.slider-nav').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  focusOnSelect: true,
  arrows: false,
  customPaging: function customPaging(slider, i) {
    return '<a href="#" class="slider-dots"></a>';
  }
});




// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// *********會務活動****************
// activity slick

$('.activity-slider').slick({
  slidesToShow: 3,
  dots: false,
  arrows: false,
  responsive: [{
    breakpoint: 1200,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      dots: true,
      customPaging: function customPaging(slider, i) {
        return '<a href="javascript:;" class="slider-dots"></a>';
      }
    }
  }, {
    breakpoint: 768,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      customPaging: function customPaging(slider, i) {
        return '<a href="javascript:;" class="slider-dots"></a>';
      }
    }
  }]
});

// *********End 會務活動****************
// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// *********董事長專區****************

// chairman slick
$('.chairman-slider').slick({
  slidesToShow: 4,
  dots: false,
  swipe: true,
  arrows: true,
  nextArrow: "<div class=\"arrows bg-primary nextArrow\">\n        <i class=\"fas fa-angle-right text-white\"></i>\n      </div>",
  prevArrow: "<div class=\"arrows bg-primary prevArrow\">\n       <i class=\"fas fa-angle-left text-white\"></i>\n     </div>",

  responsive: [{
    breakpoint: 1230,
    settings: {
      slidesToShow: 3
    }
  }, {
    breakpoint: 1030,
    settings: {
      slidesToShow: 2
    }
  }, {
    breakpoint: 768,
    settings: {
      slidesToShow: 1
    }
  }]
});

// *********End  董事長專區****************
// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// *********相關連結****************
// 首頁 related-links vue

// var related_links = new Vue({
//   el: ".related-links",
//   data: {
//     links: []
//   },
//   methods: {
//     getData: function getData() {
//       var vm = this;
//       $.get("./src/dist/js/links-item.json").then(function (data) {
//         console.log(data);
//         vm.links = data;
//       });
//     }
//   },
//   mounted: function mounted() {
//     this.getData();
//   }
// });

// *********End 相關連結************
// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// *********組織條例****************
// low-list vue
// var low_list = new Vue({
//   el: ".low-list",
//   data: {
//     lowlist: []
//   },
//   methods: {},
//   mounted: function mounted() {
//     var vm = this;
//     $.get("./src/dist/js/low-list.json").then(function (data) {
//       console.log(data);
//       vm.lowlist = data;
//     });
//   }
// });

// *********End 組織條例************
// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


///＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// ***********理監事簡介*************
// var supervisor_list = new Vue({
//   el: ".supervisor",
//   data: {
//     visorlist: [],
//     state: false
//   },
//   methods: {
//     getData: function getData() {
//       var vm = this;
//       $.get("./src/dist/js/supervisor-list.json").then(function (data) {
//         console.log(data);
//         vm.visorlist = data;
//       });
//     }
//   },
//   mounted: function mounted() {
//     this.getData();
//   }
// });

// *******end 理監事簡介*************
///＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


$(".page-numbers").addClass("page-link");
$(".current").parents(".page-item").addClass("active");
$(".pagination").css("opacity", "1");
