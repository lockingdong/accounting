<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */
$page_name = "協會新聞";
$middle_pages = json_decode('[
	{
		"name": "協會新聞",
		"url": "'. get_the_permalink(72) .'"
	}
]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

<style type="text/css">
	.news-date{
		width: 60px!important
	}
</style>

<section class="container content-wrapper">
  <div class="row">
    <div class="col-md-4 col-12 order-md-1 order-2">


      <div class="nav-classification">
        <h5 class="nav-title">最新消息</h5>
        <ul class="px-0">
		  <?php
				$the_query = new WP_Query(array(
					'posts_per_page'=>4,
					'post_type' => 'post',
					'ignore_sticky_posts' => 1,
				));
			?>
			<?php while ($the_query -> have_posts()) : 
				$the_query -> the_post(); 
			?>
			<li>
				<a href="<?php echo get_permalink() ;?>">
					<span class="nav-item-content"><?php the_title(); ?></span>
              		<p class="nav-news-timestamp">
					  <?php echo get_the_date( 'Y' )-1911; ?>年
					  <?php echo get_the_date( 'm' ); ?>月
					  <?php echo get_the_date( 'd' ); ?>日
					</p>
					
					
				</a>
			</li>
		<?php
			endwhile;
			wp_reset_postdata(); 
		?>
        </ul>
      </div>
	  


      <div class="nav-classification">
        <h5 class="nav-title">熱門消息</h5>
        <ul class="px-0">




			<?php 
				$args2 = array(
					'post_type' => 'post',
					'meta_key' => 'views_total',
					'orderby' => 'meta_value_num',
					'order' => 'DESC',
					'posts_per_page' => 4,
					'ignore_sticky_posts' => 1,
				);
				$top_posts = new WP_Query($args2);
			?>
			<?php while ($top_posts -> have_posts()) : 
				$top_posts -> the_post(); 
			?>
			
			<li>
				<a href="<?php echo get_permalink() ;?>"> 
					<span class="nav-item-content"><?php the_title(); ?></span>
					<p class="nav-news-timestamp">
						<?php echo get_the_date( 'Y' )-1911; ?>年
						<?php echo get_the_date( 'm' ); ?>月
						<?php echo get_the_date( 'd' ); ?>日
					</p>
				</a>
			</li>
			<?php
				endwhile;
				wp_reset_postdata(); 
			?>
		  
          
          


        </ul>
      </div>
      <div class="nav-classification">
        <h5 class="nav-title">標籤索引</h5>
        <div class="row">
		  <?php 
			$categories = get_categories();
			foreach($categories as $category) {
			echo '<div class="col-xl-5  col-md-12 col-sm-5 news-tags"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
			}

		  ?>
          
        </div>
      </div>
    </div>


	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>


    <div class="col-md-8 col-12 order-md-2 order-1 news-inner">
      <div class="row inner-content-title">
        <div class="col-lg-1 col-2">
          <div class="news-date">
            <div class="new-num text-center bg-secondary ">
              <h6 class="mb-0"><?php echo get_the_date("d"); ?></h6>
            </div>
            <div>
              <div class="new-mon text-center bg-primary text-nowrap">
                <h6><?php echo get_the_date("M"); ?></h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-11 col-10 px-0 my-auto">
          <h3 class="page-content-title"><?php the_title(); ?></h3>
        </div>
      </div>
      <?php 
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		if(!empty($post_thumbnail_id)) :?>
		<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
		<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
		<img src="<?php echo $img_ar[0];?>"
			alt="<?php echo $img_alt;?>"
		/>
	  <?php endif; ?>
      <!-- <p>景很認請數？我著一、為一們低也自統理時話給說正風發試媽大中然問我始否多然國團會間小意，場爭持面呢也養看處還去接性蘭再我制人實孩演力改！</p>
      <p>綠適程刻，愛品開，你盡大義態室安樂時慢術國平如成流林全沒麼、更包過上。可美可沒方。</p>
      <blockquote>
      決用軍造明任元一好，國習一！手法南！有開內，的負上準。流身歌在政成又們獨：不常發麼能科和……是頭景畫當新夜會他、陽產品約幾我情臉改能答治在的易。動去之自是，主制引買幾不把！車工長南、大於議快用毒起腦像一：狀想導乎房去遊上包，樣前人河環備方流、我數檢不這上現手個已？心院下部別的教
      </blockquote>
      <p>
      產生沒有意義的文章，供排版時填充版面用，但不會因為字義而左右版型的判斷。文字、標點符號出現機率大致符合台灣文章習慣。
      </p> -->
	  <?php the_content(); ?>

		<div>
		<?php if( get_field('attach') ): ?>
			<a class="filedown" href="<?php echo get_field('attach')['url']; ?>">
			<i class="fa fa-paperclip" aria-hidden="true"></i>
			附件：
				<?php echo get_field('attach')['title']; ?>
			</a>
		<?php endif; ?>
		</div>
      <div class="col-12">
        <div class="row piece-writing">
          <div class="col-6">
            <!-- <a href="">
              <p class="prev"><i class="fas fa-arrow-left"></i>上一篇</p>
              <p>【台北場次一】107年7月20「經濟部投資審議委員會投資業務</p>
            </a> -->

			<?php 
				$prev_post = get_adjacent_post(false, '', true);
				if(!empty($prev_post)):
			?>
				<a href="<?php echo get_permalink($prev_post->ID) ;?>">
					<p class="prev"><i class="fas fa-arrow-left"></i>上一篇</p>
					<p><?php echo $prev_post->post_title ;?></p>
				</a>
			
			<?php else: ?>
				
				<p class="next">沒有上一篇了！</p>
			
			<?php endif; ?>
          </div>
          <div class="col-6">
        
			<?php 
				$next_post = get_adjacent_post(false, '', false);
				if(!empty($next_post)):
			?>
				<a href="<?php echo get_permalink($next_post->ID) ;?>">
					<p class="next">下一篇<i class="fas fa-arrow-right"></i></p>
					<p><?php echo $next_post->post_title ;?></p>
				</a>
			<?php else: ?>
				
					<p class="next">沒有下一篇了！</p>
					
				
			<?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
			
  <?php endwhile; ?>
  <?php endif; ?>




</section>





<?php
//get_sidebar();
get_footer();
