<?php
/**
  Template Name: 本會簡介-組織圖
 */
global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

<section class="container content-wrapper organization-content">
  <h3 class="content-title"><?php echo get_field('map_title'); ?></h3>
  <span class="star-symbol">★★★★★</span>
  
  <!-- <img src="<?php //echo bloginfo("stylesheet_directory"); ?>/src/dist/images/organizationmap.jpg" alt=""> -->
  <?php echo get_field('map_content'); ?>
</section>
	

<?php
//get_sidebar();
get_footer();
