<?php
/**
  Template Name: 本會簡介-宗旨
 */

global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	}

]');
$page_title = get_the_title();
get_header(); ?>
<style>
	.img-wrap {
		position: relative;
		padding-bottom: 38%;
		overflow: hidden;
		width: 50%;
		float: left;
		margin: 0 10px;

	}
	@media (max-width: 991px){
		.img-wrap {
			
			padding-bottom: 56.25%;
			overflow: hidden;
			width: 100%;
			margin-left: 0;
			margin-right: 0;
			margin-bottom: 20px;

		}

	}
	.img-wrap img {
		position: absolute;
		max-height: none !important;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
</style>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<section class="container  content-wrapper purpose">
  <div class="purpose-wrapper clearfix">


<?php if(get_field("left_main_img")): ?>
  	<div class="img-wrap">
  		<img src="<?php echo get_field("left_main_img")['url']; ?>" 
		 alt="<?php echo get_field("left_main_img")['alt']; ?>">
  		
  	</div>
<?php endif; ?>
  	





		 <!-- <h3 class="content-title"><?php //echo get_field('first_title'); ?></h3>
			<span class="star-symbol">★★★★★</span>
			<p><?php //echo get_field('first_content'); ?></p>
			<br>
			<br> -->

	<?php if( have_rows('right_contents') ): ?>
		<?php while( have_rows('right_contents') ): the_row(); ?>


		<h3 class="content-title"><?php echo get_sub_field('title'); ?></h3>
		<span class="star-symbol">★★★★★</span>
		<p><?php echo get_sub_field('content'); ?></p>
		<br>
		<br>	

		<?php endwhile; ?>
	<?php endif; ?>



	
    <!-- <h3 class="content-title">本會沿革</h3>
    <span class="star-symbol">★★★★★</span>
    <p>本會以聯合會計師公會共同闡揚會計審計學術，發揮會計師功能，促進會計師制度，並協助國家社會財經建設，增進國際間會計審計學術之交流，共謀發展會計師事業為宗旨。</p>
    <p>本會以聯合會計師公會共同闡揚會計審計學術，發揮會計師功能，促進會計師制度，並協助國家社會財經建設，增進國際間會計審計學術之交流，共謀發展會計師事業為宗旨。</p>
    <p>本會以聯合會計師公會共同闡揚會計審計學術，發揮會計師功能，促進會計師制度，並協助國家社會財經建設，增進國際間會計審計學術之交流，共謀發展會計師事業為宗旨。發揮會計師功能，促進會計師制度，並協助國家社會財經建設，增進國際間會計審計學術之交流，共謀發展會計師事業為宗旨。</p>
  </div>-->
</section> 

<?php
//get_sidebar();
get_footer();
