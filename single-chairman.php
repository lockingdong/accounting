<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */
$page_name = "理事長專區";
$middle_pages = json_decode('[

]');
$page_title = "理事長專區";
get_header(); ?>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<section class="container content-wrapper">
 <div class="row">
   <div class="col-lg-6 col-12">
     <div class="chairman-img">
		<?php 
			$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
			if(!empty($post_thumbnail_id)) :?>
			<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
			<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
			<img src="<?php echo $img_ar[0];?>"
				alt="<?php echo $img_alt;?>"
			/>
		<?php endif; ?>
       <div class="chairman-info">
         <span><?php the_title() ; ?></span> <p class="mb-0"><?php echo get_field("full_name") ; ?></p>
       </div>
     </div>
   </div>
   <div class="col-lg-6 col-12">
     <div class="experience">
       <h5><?php echo get_field("title1") ; ?></h5>
       <?php echo get_field("content1") ; ?>
     </div>
     <div class="experience">
       <h5><?php echo get_field("title2") ; ?></h5>
       <?php echo get_field("content2") ; ?>
     </div>
     <div class="experience" style="margin-bottom:0px;">
       <h5><?php echo get_field("title3") ; ?></h5>
       <?php echo get_field("content3") ; ?>
     </div>
   </div>
 </div>
 <div class="chairman-msg" style="margin-top:50px;">
   <h4><?php echo get_field("title4") ; ?></h4>
   <?php echo get_field("content4") ; ?>
 </div>
</section>
	
	



<?php
get_footer();
