<?php
/**
  Template Name: test
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->


	<?php 
		// echo json_encode(get_field("test_field"));

		// $table = TablePress::$controller->model_table->load( 2 );
		// echo "<script> var table_data = " .json_encode( $table['data']). "</script>";

		// add_filter( 'tablepress_table_output', 'mjharris2407_tablepress_json_output', 2, 3 );
		// function mjharris2407_tablepress_json_output( $output, $table, $render_options ) {
		// 	$output = "<script> var table_data = " . json_encode( $table['data'] ). "</script>";
		// 	return $output;
		// }
	; ?>

	<?php
	$datas = TablePress::$controller->model_table->load(3);
	$datas = $datas['data'];


	//echo json_encode($datas);
	?>

	<?php 
	$array = [];
	foreach ($datas as $data){
		
		$obj->id = $data[0];
		$obj->name = $data[1];
		$obj->vale = $data[2];
		array_push($array, $obj);
		unset($obj);
	}
	?>
	<?php 
		echo json_encode($array);
	?>


	
	<?php
	// $myObj->name = "John";
	// $myObj->age = 30;
	// $myObj->city = "New York";

	// $myJSON = json_encode($myObj);

	// echo var_dump($myObj);
	?>
	

<?php
//get_sidebar();
get_footer();
