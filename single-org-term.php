<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */

global $post;
$a_id=$post->post_author;

$page_name = "本會簡介";
$middle_pages = json_decode('[
	{
		"name": "'. get_the_author_meta( 'nickname', $a_id ) .'",
		"url": "#"
	},
	{
		"name": "組織條例",
		"url": "'. get_home_url(). "/組織條例-" . get_the_author_meta( 'nicename', $a_id ) .'" 
	}

]');
$page_title = get_the_title();

get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>



	


<section class="container content-wrapper row-list-content">
  <h3 class="content-title"><?php the_title() ; ?></h3>
  <span class="star-symbol">★★★★★</span>
  <?php echo get_field("term_detail") ; ?>

<?php if( get_field('attachment') ): ?>
	<a class="filedown" href="<?php echo get_field('attachment')['url']; ?>">
	<i class="fa fa-paperclip" aria-hidden="true"></i>
	附件：
		<?php echo get_field('attachment')['title']; ?>
	</a>
<?php endif; ?>
</section>

<?php
get_footer();
