<?php
/**
  Template Name: 首頁
 */

get_header(); ?>
<style>
	.banner-img-wrap {
    padding-bottom: 56.25%;
    position: relative;
    overflow: hidden;
    display: block;
  }

  .banner-img-wrap img {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%) scale(1) !important;
    width: 100%;
  }

  .activity-slider .slick-slide > div {
    width: 100% !important;
  }

  .activity-slider .slick-track .slick-slide div {
    width: 100%;
  }

  .activity .container .activity-slider{
    display: block!important;
  }

  .news-info-area a{
    padding-bottom: 64%;
  }
  .img-home-wrap {
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    min-height: 300px;
    padding-bottom: 60%;
  }
</style>

<section class="container-fluid">
  <div class="row slider-kv" >
    <div class="col-lg-4  col-12 p-0 slider-wrapper-m" >
      <div class="slider-nav slider-nav-bg d-flex flex-column  justify-content-center">

	<?php if( have_rows('top_banner') ): ?>
		<?php while( have_rows('top_banner') ): the_row(); ?>

        <div class="slider-info m-auto ">
          <h3><?php echo get_sub_field('title'); ?></h3>
          <p><?php echo get_sub_field('content'); ?></p>
        </div>

		<?php endwhile; ?>
	<?php endif; ?>
	

      </div>
    </div>
    <div class="col-lg-8 col-12  p-0">
      <div class="slider-for" style="font-size:0;">

	  <?php if( have_rows('top_banner') ): ?>
		<?php while( have_rows('top_banner') ): the_row(); ?>
        <div>
          <a class="banner-img-wrap" 
             href="<?php echo get_sub_field('url'); ?>"
             style="
                background-image:url(<?php echo get_sub_field('image')['url']; ?>);
                background-size:cover;
                background-repeat:no-repeat;
                background-position: center center;
                min-height:300px;
                "
             
          >
		    <!-- <img class="img-fluid" src="<?php //echo get_sub_field('image')['url']; ?>" 
				 alt="<?php //echo get_sub_field('image')['alt']; ?>" -->
				 
			>
		  </a>
        </div>
		<?php endwhile; ?>
	  <?php endif; ?>
        
      </div>
    </div>
  </div>
</section>



<section class="container-fluid introduction">
  <div class="row introduction-tabs">
    <div class="introduction-tab-item text-nowrap" v-for="(tab,idx) in tabs"  :key="idx" >
      <div class="introduction-tab-item-img">
       <img :src='"<?php bloginfo('stylesheet_directory'); ?>/src/dist/images/logo/"+tab.title' alt="logo">
      </div>
      <h2>{{tab.title}}</h2>
    </div>
  </div>
  <div class="container  introduction-info">
    <div class="row">
      <div :class="{'col-lg-6': items.img!=null}" class="order-lg-1 order-2">
        <div class="page-title introduction-title">
          <h2><span class="text-secondary">關於</span>本會成立宗旨</h2>
        </div>
        <div class="introduction-content" >
          <div  v-html="items.content">
          </div>
          <a :href="items.href" class="btn btn-primary btn-hover introduction-btn">暸解更多<i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
      <!-- {{items.img}} -->
      <div :class="{'col-lg-6':(items.img!=null)}" class="col-sm-12 col-12 m-auto order-lg-2 order-1" v-if="items.img">
        <div class="img-home-wrap"
             :style="{backgroundImage: 'url(' + items.img + ')'}"      
        >
          <!-- <img :src="items.img" alt="#" class="img-fluid"> -->
        </div>
      </div>
    </div>
  </div>
</section>




<section class="container-fluid news">
  <div class="container news-wrapper">
    <h2 class="news-title text-lg-left text-center">最新<span class="text-secondary">消息</span></h2>
    <div class="row">
      <div class="col-lg-6 col-12">

		<?php
      //$cur = get_query_var('paged');
      $stickies = get_option( 'sticky_posts' );
			$the_query = new WP_Query(array(
				'post_type'           => 'post',
        'post__in'            => $stickies,
        'posts_per_page'      => 1,
        'ignore_sticky_posts' => 1
			));
		?>
		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>
        <div class="news-info-area">
        <?php 
          $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
          if(!empty($post_thumbnail_id)) :?>
          <?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
				  <?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
          <a href="<?php echo get_permalink() ;?>"
             style="display:block;
                    background-image:url(<?php echo $img_ar[0];?>);
                    background-size:cover;
                    background-repeat:no-repeat;
                    background-position: center center;
                    
             "
          >
			
			<?php endif; ?>
          </a>
          <div class="news-list">
            <div class="news-date">
              <div class="new-num text-center bg-secondary ">
                <h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
              </div>
              <div>
                <div class="new-mon text-center bg-primary text-nowrap">
                  <h6><?php echo get_the_date( 'M' ); ?></h6>
                </div>
              </div>
            </div>
            <div class="news-list-title">
              <a href="<?php echo get_permalink() ;?>">
                <h5><?php echo wp_trim_words(get_the_title(), 30, '...'); ?></h5>
              </a>
            </div>
          </div>
          <a href="<?php echo get_permalink() ;?>">
            <div class="news-linearbg"></div>
          </a>
        </div>
		<?php
			endwhile;
			wp_reset_postdata(); 
		?>


      </div>




      <div class="col-lg-6 col-12">
        <div class="news-list-wrapper d-flex flex-column justify-content-around ">
          
		  
		  
		  
		  
		<?php
			$cur = get_query_var('paged');
			$the_query = new WP_Query(array(
				'posts_per_page'=>3,
				'ignore_sticky_posts' => 1,
				'post_type' => 'post'
			));
		?>

		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>
		  
		  
		  <!-- /// -->
		  <div class="news-list">
            <div class="news-date">
              <div class="new-num text-center bg-secondary ">
                <h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
              </div>
              <div>
                <div class="new-mon text-center bg-primary text-nowrap">
                  <h6><?php echo get_the_date( 'M' ); ?></h6>
                </div>
              </div>
            </div>
            <div class="news-list-title">
              <a href="<?php echo get_permalink() ;?>"
				post_id="<?php echo get_the_ID() ;?>"
				class="news-link"
				>
                <h5><?php echo wp_trim_words(get_the_title(), 30, '...'); ?></h5>
              </a>
            </div>
          </div>
		  <!-- /// -->

		<?php
			endwhile;
			wp_reset_postdata(); 
		?>





          
        </div>
        
        <p class="text-right pr-lg-2 news-more see-more">
          <a href="<?php echo get_the_permalink(72); ?>">查看更多<i class="fas fa-arrow-right"></i></a>
        </p>
      </div>
    </div>
  </div>
  <div class="news-mask"></div>
</section>



<section class="container-fluid activity">
  <div class="row">
    <div class="container">
      <div class="page-title">
        <h2>會務<span class="text-secondary">活動</span></h2>
        <p class="text-right mb-0 see-more">
          <a href="<?php echo get_post_type_archive_link( 'act' ); ?>">查看更多<i class="fas fa-arrow-right"></i></a>
        </p>
      </div>
      <div class="row activity-slider">
		


		<?php
			
			//$cur = get_query_var('paged');

			$the_query = new WP_Query(array(
				'posts_per_page'=>3,
        'post_type' => 'act',
        //'comment_status' => 'open',
        'tax_query' => array(
          array (
              'taxonomy' => 'act-type',
              'field' => 'slug',
              'terms' => 'on_home',
          )
        )
				//'paged' => $cur
			));

		?>

		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>

        <div class="card">
          <div class="card-img">
            <a href="<?php echo get_permalink() ;?>">
			<?php 
				$post_thumbnail_id2 = get_post_thumbnail_id( $post->ID );
				if(!empty($post_thumbnail_id2)) :?>
				<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id2, 'full' ); ?>
				<?php $img_alt =  get_post_meta($post_thumbnail_id2, '_wp_attachment_image_alt', true); ?>
				<img src="<?php echo $img_ar[0];?>"
					alt="<?php echo $img_alt;?>"
				/>
			<?php endif; ?>
            </a> 
          </div>
          <div class="card-body">
            <h5 class="card-title text-secondary">
				<?php echo get_the_date( 'Y' )-1911; ?>年
				<?php echo get_the_date( 'm' ); ?>月
				<?php echo get_the_date( 'd' ); ?>日/滿額
				<?php echo get_field("total") ?>人</h5>
            <p class="card-text"><?php the_title(); ?></p>
            <a href="<?php echo get_permalink() ;?>" class="btn btn-primary ">暸解更多<i class="fas fa-arrow-right"></i></a>
          </div>
        </div>

		<?php
			endwhile;
			wp_reset_postdata(); 
		?>
        




      </div>
    </div>
  </div>
</section>

<section class="container-fluid chairman">
  <div class="row">
    <div class="container">
      <div class="page-title">
        <h2>理事長<span class="text-secondary">專區</span></h2>
      </div>
      <div class="row chairman-slider">

	  <?php
		$the_query = new WP_Query(array(
			'posts_per_page'=>999,
			'post_type' => 'chairman',
		));
	  ?>
	  <?php while ($the_query -> have_posts()) : 
		$the_query -> the_post(); 
	  ?>


        <div class="card">
        <a href="<?php echo get_permalink() ;?>">

          <?php 
            $post_thumbnail_id3 = get_post_thumbnail_id( $post->ID );
            if(!empty($post_thumbnail_id3)) :?>
            <?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id3, 'full' ); ?>
            <?php $img_alt =  get_post_meta($post_thumbnail_id3, '_wp_attachment_image_alt', true); ?>
          <?php else: ?>
            <?php $img_ar = ''; ?>

          <?php endif; ?>
          <div class="card-img"
              style="
                    background-image:url(<?php echo $img_ar[0];?>);
                    background-size:cover;
                    background-repeat:no-repeat;
                    background-position: center center;
                    min-height:300px;
                    "
          >
            
          
            
            </div>
          </a> 
          <div class="card-body bg-primary text-center">
            <h5 class="text-white chairman-name"><?php the_title(); ?></h5>
            <h6 class="text-white chairman-position"><?php echo get_field('full_name'); ?></h6>
          </div>
        </div>


	<?php
		endwhile;
		wp_reset_postdata(); 
	?>
        
      </div>
    </div>
  </div>
</section>

<section class="container-fluid related-links" id="index-related-links">
  <div class="row">
    <div class="container">
      <div class="page-title">
        <h2>相關<span class="text-secondary">連結</span></h2>
      </div>
      <div class="row">

		<?php if( have_rows('all_links', 139) ): ?>
			<?php while( have_rows('all_links', 139) ): the_row(); ?>

      <?php if(get_sub_field('on_home')): ?>
        <div class="col-lg-4 col-sm-6 col-8  text-left ">
          <p class="text-nowrap">
            <a href="<?php echo get_sub_field('link_url'); ?>" target="_blank">
              <span class="arrow-icon bg-primary">
			  	<i class="fas fa-angle-right text-white"></i>
			  </span>
			  <?php echo wp_trim_words(get_sub_field('link_name'), 20, '...'); ?>
            </a> 
         </p>
        </div>
      <?php endif; ?>

			<?php endwhile; ?>
		<?php endif; ?>

      </div>
    </div>
  </div>
</section>
<div id="app"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script>

// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
// *********本會簡章****************
var introduction = new Vue({
  el: ".introduction",
  data: {
    active: 0,
    tabs: [],
    items: [],
    hasimg: false
  },
  computed: {},
  methods: {
    activate: function activate(idx) {
      this.active = idx;
      this.items = this.tabs[this.active];
    },
    getData: function getData() {
      var vm = this;
      $.getJSON("api/").then(function (data) {
        vm.tabs = data;
      }).then(function (data) {
        console.log(data);
        vm.setdata();
      });
    },
    setdata: function setdata() {
      var vm = this;
      $('.introduction-tabs').slick({
        slidesToShow: 7,
        dots: false,
        arrows: false,
        focusOnSelect: true,
        infinite: true,
        // swipe:true,
        swipeToSlide: true,
        responsive: [{
          breakpoint: 1630,
          settings: {
            slidesToShow: 6
          }
        }, {
          breakpoint: 1440,
          settings: {
            slidesToShow: 5
          }
        }, {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 950,
          settings: {
            slidesToShow: 3
          }
        }, {
          breakpoint: 700,
          settings: {
            slidesToShow: 2
          }
        }]
      });
      vm.items = vm.tabs[vm.active];
    }
  },
  mounted: function mounted() {
    this.getData();
  },
  updated: function updated() {
    var vm = this;
    $('.introduction-tabs').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      console.log(slick);
      vm.activate(nextSlide);
    });

    limit_text(".introduction-content p", 200);
  }
});

// *********End 本會簡章****************
// ＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


function limit_text(text_obj, limit_num){
	$(text_obj).each(function(){
	//alert();
	//console.log($(this).text().length);
        var x = $.trim($(this).html().replace(/ /g,""));
        var y = x.replace(/\n/g,"");
		//var h3_number = $(this).html().length;
        var h3_number = y.length;
		if(h3_number > limit_num){
			//var text_board_right_limit_h3 = $(this).html().substring(0, limit_num);
            var text_board_right_limit_h3 = y.substring(0, limit_num);
			//console.log(text_board_right_limit_h3)
            if(text_board_right_limit_h3.slice(-1)=="<"){
                text_board_right_limit_h3 = text_board_right_limit_h3.slice(0, -1)
            }
			$(this).html(text_board_right_limit_h3 + '...');
		}
        //console.log(y)
	});
}

</script>

	
<?php
//get_sidebar();
get_footer();
