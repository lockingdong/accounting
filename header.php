<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>


	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="format-detection" content="telephone=no">
  <meta name="keywords" content="稅務,稅務代理人,稅務代理人協會,中華稅務代理人協會,台北稅務代理人協會,桃園稅務代理人協會,台中稅務代理人協會,台南稅務代理人協會,高雄稅務代理人協會,新北稅務代理人協會">
  <meta name="description" content="稅務代理人協會,中華稅務代理人協會">
	<link rel="shortcut icon" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/images/accountant.ico" type="image/x-icon">
	<link rel="stylesheet" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/all.min.css">
	<link rel="stylesheet" href="<?php bloginfo("stylesheet_directory"); ?>/src/dist/css/fix.css">
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
	<!-- <title>首頁｜會計師工會</title> -->





<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>




	
<header class="container-fluid ">
  <div class="row bg-primary navbar-info ">
    <div class="container p-0">
      <div class="col-12 mx-auto d-flex justify-content-sm-start justify-content-center">
        <p class="m-0">
          <i class="fas fa-phone"></i> <a href="02-2392-5077"></a>
          <?php echo get_field("phone_num", 224); ?>
        </p>
        <p class="m-0">
          <i class="fas fa-map-marker-alt"></i>
          <?php echo get_field("address", 224); ?>
        </p>
      </div>
    </div>
  </div> 
  <div class="row bg-white navbar-bg" >
    <div class="navbar-wrapper p-0">
      <nav class="navbar navbar-expand-lg p-0 "  >
        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          <img src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/images/LOGO.svg" alt=""><h1 class="">稅務代理人協會</h1>
        </a>
        <div class="navbar-toggler special-button" data-toggle="collapse" data-target="#navbarNavDropdown">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 150 150" width="50" xml:space="preserve">
            <path class="st0" d="M41,78h71c0,0,29.7-8,16-36c-1.6-3.3-19.3-30-52-30c-36,0-64,29-64,65c0,29,26,59,61.6,59
            c41.4,0,62.6-29,62.6-59c0-37.7-19.2-49.1-26.3-54"
                stroke-linejoin="round" 
                stroke-linecap="round"/>
          </svg>
          <div class="top"></div>
          <div class="bottom"></div>
        </div>
        <!-- 選單 -->
        <?php get_template_part("template-parts/content", "nav"); ?>
      </nav>
    </div>
  </div>
</header>
<div class="navbar-top"></div>

	<!-- <div id="content" class="site-content"> -->









	
