<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */
$page_name = "申請入會";
$middle_pages = json_decode('[
	{
		"name": "會員名單",
		"url": "'. get_the_permalink(147) .'"
	}
]');
$page_title = "申請入會";
get_header(); ?>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

	<!-- <header class="page-header">
		<?php
			//the_archive_title( '<h1 class="page-title">', '</h1>' );
			//the_archive_description( '<div class="archive-description">', '</div>' );
		?>
		<?php 
			//$obj = get_queried_object();
			//$cate_name =  $obj -> {"category_nicename"};
		?>
	</header> -->


	<?php
		// $latest_tags_count = get_field('latest_tags_count');
		// $hot_tags_count = get_field('hot_tags_count');
		$cur = get_query_var('paged');

		$the_query = new WP_Query(array(
			'posts_per_page'=>3,
			'post_type' => 'register',
			'paged' => $cur
		));

	?>

	

<section class="container content-wrapper">
  
<?php while ($the_query -> have_posts()) : 
	$the_query -> the_post(); 
?>




  <div class="menbership-apply-list">
    <h3 class="content-title"><?php the_title(); ?></h3>
    <span class="star-symbol">★★★★★</span>


    

	<?php if( have_rows('files') ): ?>
		<?php while( have_rows('files') ): ?> 
			<?php the_row();?>



			<div class="menbership-apply-item">
			<h4><?php echo get_sub_field('file_name') ; ?></h4>
			<div class="row">
				<div class="col-md-10"><p><?php echo get_sub_field('description') ; ?></p></div>
				<div class="col-md-2 text-right">
				<a class="btn btn-primary btn-download" href="<?php echo get_sub_field('file')['url']; ?>" download >下載</a>
				</div>
			</div>
			</div>

			


			
		<?php endwhile; ?>
	<?php endif; ?>

  </div>



  <?php
	endwhile;
	wp_reset_postdata(); 
?>




  <!-- <nav aria-label="Page navigation example ">
    <ul class="pagination justify-content-center">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <li class="page-item active"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">4</a></li>
      <li class="page-item"><a class="page-link" href="#">5</a></li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav> -->
  <nav aria-label="Page navigation example ">
		<?php my_pagination(); ?>
	</nav>
</section>

		

<?php
//get_sidebar();
get_footer();
