<?php
/**
  Template Name: 協會新聞-熱門消息集結頁
 */
$page_name = "協會新聞";
$middle_pages = json_decode('[

]');
$page_title = get_the_title();
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<section class="container-fluid content-wrapper ">		
	<article class="container content-wrapper news-list-category">  
		

		<?php

			$per_page = get_field('hot_per_page');
			$args2 = array(
				'post_type' => 'post',
				'meta_key' => 'views_total',
				'orderby' => 'meta_value_num',
				'order' => 'DESC',
				'posts_per_page' => $per_page,
				'ignore_sticky_posts' => 1
			);

			$top_posts = new WP_Query($args2);

			
		?>

		<?php while ($top_posts -> have_posts()) : 
			$top_posts -> the_post(); 
		?>

		
		


	<div class="row news-info-items">
		<div class="col-md-1 col-2 my-auto px-0">
			<div class="news-date">
			<div class="new-num text-center bg-secondary ">
				<h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
			</div>
			<div>
				<div class="new-mon text-center bg-primary text-nowrap">
				<h6><?php echo get_the_date( 'M' ); ?></h6>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-11 col-9 px-0 news-info-line">
			<div class="row">
			<div class="col-10 px-0 ">
				<div class="news-list">
				<div class="news-list-title">
					<a href="<?php echo get_permalink() ;?>"
					post_id="<?php echo get_the_ID() ;?>"
					class="news-link"
					>
					
					<h5><?php echo wp_trim_words( get_the_content(), 60, '...' ); ?></h5>

					
					</a>
					
					
				</div>
				</div>
			</div>
			<div class="col-2 my-auto news-info-right">
				<a href="<?php echo get_permalink() ;?>" class="">＞</a>
			</div>
			</div>
		</div>
	</div>

		

			
	<?php
		endwhile;
		wp_reset_postdata(); 
	?>

		

		<nav aria-label="Page navigation example ">
			<?php my_pagination(); ?>
		</nav>


	</article>



</section>

	

	<!-- <nav aria-label="Page navigation example ">
		<?php //my_pagination(); ?>
	</nav> -->


</article>

</section>












<?php
//?s=&post_date=2012-01-01+2018-09-13



//get_sidebar();

get_footer();
