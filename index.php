<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */

get_header(); ?>

	<?php
				//$author_id = get_field('author');
				$cur = get_query_var('paged');

				$the_query = new WP_Query(array(
					'posts_per_page'=>2,
					'post_type' => 'post',
					//'paged' => $cur
				));
			?>

			<?php while ($the_query -> have_posts()) : 
				$the_query -> the_post(); 
			?>

			<div style="margin: 20px;">
				<a href="<?php echo get_permalink() ;?>">
					<?php echo get_the_date( 'M' ); ?>
					<?php echo get_the_date( 'd' ); ?>
					<?php the_title( '<div>', '</div>' ); ?>
				</a>
			</div>
				


			<?php
				endwhile;
				wp_reset_postdata(); 
			?>

			<?php  
				echo paginate_links(array(
					'total' => $the_query->max_num_pages
				));
			?>

			

<?php
//get_sidebar();
get_footer();
