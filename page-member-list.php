<?php
/**
  Template Name: 會員名單
 */

$page_name = "會員名單";
$middle_pages = json_decode('[

]');
$page_title = get_the_title();
get_header(); ?>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

<style>
	.pagination li {
		list-style: none;
	}
	.pagination li.active a {
		background-color: #ddf5fb;
		border-color: #ddf5fb;
	}
	

	.pagination li a {
		padding: .5rem .75rem;
		border: 1px solid #dee2e6;
		border-left: none;
		cursor: pointer;
		color: #5894f4!important;
	}
	.pagination li a:hover {
		background-color: #e9ecef;
	}

	.pagination li:nth-child(1) a {
		padding: .5rem .75rem;
		border: 1px solid #dee2e6;
		border-radius: .25rem 0 0 .25rem;
	}
	.pagination li:last-child a {
		
		border-radius: 0 .25rem .25rem 0;
	}
	.pagination li.active a:hover{
		background: #ddf5fb !important;
	    color: #5894f4!important;
	}
</style>


<section class="bg-secondary apply-section">
  <div class="container">
    <p class="mb-0">
      想要加入協會，立即申請入會 
      <a href="<?php echo get_post_type_archive_link( 'register' ); ?>" class="btn btn-primary">立即申請</a>
    </p>
  </div>
</section>


<div>
	<!-- <select id="com">
		<option value="">所有</option>
		<option value="中華">中華稅務代理人協會</option>
		<option value="台北">台北市稅務代理人協會</option>
		<option value="新北">新北市稅務代理人協會</option>
		<option value="桃園">桃園市稅務代理人協會</option>
		<option value="台中">台中市稅務代理人協會</option>
		<option value="台南">台南市稅務代理人協會</option>
		<option value="高雄">高雄市市稅務代理人協會</option>
	</select>
	<input type="text" id="member">

	<button class="search-btn">
		search
	</button> -->
	
</div>




<?php 
//http://www.example.com/?id=2
	$com = $_GET['com'];
	$member = $_GET['member'];
	//$com = $_GET['com'];
?>

<?php

//tw中華-會員名單
	$tw_datas = TablePress::$controller->model_table->load(3);

	$tpc_datas = TablePress::$controller->model_table->load(5);

	$ntpc_datas = TablePress::$controller->model_table->load(4);

	$tyh_datas = TablePress::$controller->model_table->load(6);

	$tcc_datas = TablePress::$controller->model_table->load(7);

	$tnh_datas = TablePress::$controller->model_table->load(10);

	$khc_datas = TablePress::$controller->model_table->load(11);

	//$all_datas = TablePress::$controller->model_table->load(5);


	$merged_datas = array_merge($tw_datas['data'],$tpc_datas['data'],$ntpc_datas['data'],$tyh_datas['data'],$tcc_datas['data'],$tnh_datas['data'],$khc_datas['data']);

	//echo var_dump($tw_datas);
	$datas = $merged_datas;
	//echo var_dump($datas);
?>
<?php 
	$array = []; //origin array
	foreach ($datas as $data){
		
		$obj->name = $data[0];
		$obj->org = $data[1];
		$obj->company = $data[2];
		$obj->phone = $data[3];
		$obj->fax = $data[4];
		$obj->mail = $data[5];
		$obj->address = $data[6];
		array_push($array, $obj);
		unset($obj);
	}
?>

<?php 
	// $s1 = "中華";
	// $s2 = "中華稅務";
	function match($s1, $s2){
		if (preg_match("/$s1/i", "$s2")) {
			return true;
		} else {
			return false;
		}
	}

	//echo match("華", "中華稅務");
?>
<?php 
	$new_array = [];
	foreach($array as $item){
		$is_match = match($com, $item->{'org'});
		$is_match2 = match($member, $item->{'name'});
		//echo $is_match;
		//unset($is_match);
		if($is_match == true && $is_match2 == true){
			array_push($new_array, $item);
		}
	}
	$array = $new_array;
?>


<!-- <div id="app">
  <paginate :per="10" name="items" :list="items" class="paginate-list">
    <li v-for="item in paginated('items')">
		<span>{{ item.name }}</span>
		<span>{{ item.org }}</span>
		<span>{{ item.company }}</span>
		<span>{{ item.phone }}</span>
		<span>{{ item.fax }}</span>
		<span>{{ item.mail }}</span>
		<span>{{ item.address }}</span>
    </li>
  </paginate>
  <div>
  <paginate-links for="items" :limit="3" :show-step-links="true"></paginate-links>
</div>
</div> -->


<section id="app" class="container-fluid content-wrapper">
  <article class="container">
    <div action="" novalidate class="search-form" >
      <div>
        <span class="search-classification">查詢</span> 
        <select id="com" class="member-search search-colum office-select">
			<option selected value="">所有</option>
			<option value="中華">中華稅務代理人協會</option>
			<option value="台北">台北市稅務代理人協會</option>
			<option value="新北">新北市稅務代理人協會</option>
			<option value="桃園">桃園市稅務代理人協會</option>
			<option value="台中">台中市稅務代理人協會</option>
			<option value="台南">台南市稅務代理人協會</option>
			<option value="高雄">高雄市市稅務代理人協會</option>
        </select>
        <input  id="member" type="text" name="member-name" placeholder="王小明"  class="member-search search-colum ">
        <button type="submit" class="btn btn-primary search-btn">查詢</button>
      </div>
    </div>
  </article>
  <hr class="separation-line">

  <paginate :per="10" name="items" :list="items" class="paginate-list container member-content">

  
    <div class="row member-info-item">
      <div class="col-1 name-td-lg">
        姓名
      </div>
      <div class="col-2 officename-td-lg">
        協會名稱
      </div>
      <div class="col-1 office-td ">
        事務所名稱
      </div>
      <div class="col-2 mobile-td">
        電話
      </div>
      <div class="col-2 mobile-td">
        傳真
      </div>
      <div class="col-2">
        信箱
      </div>
      <div class="col-2">
        地址
      </div>
    </div>



    <!-- <div class="row member-info">
      <div class="col-lg-1 name-td-lg member-flex-lg">
        <span class="member-item-mobile">姓名</span> <span class="member-info-mobile">王小萌</span>  
      </div>
      <div class="col-lg-2 officename-td-lg member-flex-lg">
        <span class="member-item-mobile">協會名稱</span> <span class="member-info-mobile">台北市稅務代理人協會</span>
      </div>
      <div class="col-lg-1 office-td member-flex-lg">
        <span class="member-item-mobile">事務所名稱</span>  <span class="member-info-mobile">台北市稅務代</span> 
      </div>
      <div class="col-lg-2 mobile-td member-flex-lg">
        <span class="member-item-mobile">電話</span> <span class="member-info-mobile">02-12345667*2122</span> 
      </div>
      <div class="col-lg-2 mobile-td member-flex-lg">
        <span class="member-item-mobile">傳真</span> <span class="member-info-mobile">02-12345667</span> 
      </div>
      <div class="col-lg-2 email-td member-flex-lg">
         <span class="member-item-mobile">信箱</span> <span class="member-info-mobile">asdsfsdfsdf@yahoo.com.tw</span> 
      </div>
      <div  class="col-lg-2 member-flex-lg">
       <span class="member-item-mobile">地址</span> <span class="member-info-mobile">新北市一案二砂四五連雕七掰柯逼哋嘚嘚殼</span>
      </div>
    </div> -->

	<li v-for="item in paginated('items')" class="row member-info">
		<!-- <span>{{ item.name }}</span>
		<span>{{ item.org }}</span>
		<span>{{ item.company }}</span>
		<span>{{ item.phone }}</span>
		<span>{{ item.fax }}</span>
		<span>{{ item.mail }}</span>
		<span>{{ item.address }}</span> -->

	  <div class="col-lg-1 name-td-lg member-flex-lg">
        <span class="member-item-mobile">姓名</span> <span class="member-info-mobile">{{ item.name }}</span>  
      </div>
      <div class="col-lg-2 officename-td-lg member-flex-lg">
        <span class="member-item-mobile">協會名稱</span> <span class="member-info-mobile">{{ item.org }}</span>
      </div>
      <div class="col-lg-1 office-td member-flex-lg">
        <span class="member-item-mobile">事務所名稱</span>  <span class="member-info-mobile">{{ item.company }}</span> 
      </div>
      <div class="col-lg-2 mobile-td member-flex-lg">
        <span class="member-item-mobile">電話</span> <span class="member-info-mobile">{{ item.phone }}</span> 
      </div>
      <div class="col-lg-2 mobile-td member-flex-lg">
        <span class="member-item-mobile">傳真</span> <span class="member-info-mobile">{{ item.fax }}</span> 
      </div>
      <div class="col-lg-2 email-td member-flex-lg">
         <span class="member-item-mobile">信箱</span> <span class="member-info-mobile">{{ item.mail }}</span> 
      </div>
      <div  class="col-lg-2 member-flex-lg">
       <span class="member-item-mobile">地址</span> <span class="member-info-mobile">{{ item.address }}</span>
      </div>
    </li>
	
   
    
   
  </paginate>
  <nav aria-label="Page navigation example ">


	<paginate-links class="pagination justify-content-center" for="items" :limit="3" :show-step-links="true"></paginate-links>
  </nav>
  
</section>
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://rawgit.com/TahaSh/vue-paginate/master/dist/vue-paginate.js"></script>
<script>
	new Vue({
		el: '#app',
		data: {
			items: <?php echo json_encode($array); ?>,
			paginate: ['items']
		}
	})


	$(".search-btn").on('click', function(){
		//window.location.href = `<?php echo get_permalink();?>?com=${$("#com").val()}&member=${$("#member").val()}`;
		window.location.href = "<?php echo get_permalink();?>"+"?com="+$("#com").val()+"&member="+$("#member").val();
	})
	$("#com").val("<?php echo $com;?>")
	$("#member").val("<?php echo $member;?>")

</script>
<?php
//get_sidebar();
get_footer();
