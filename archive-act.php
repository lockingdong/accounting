<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */
$page_name = "會務活動";
$middle_pages = json_decode('[

]');
$page_title = "會務活動";
get_header(); ?>
<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>


<style>
	.end {
		position: relative;
	}
	.end:before{
		letter-spacing: 3px;
		font-size: 22px;
		display: flex;
		color: white;
		font-weight: 300;
		content: "活動截止";
		position: absolute;
		justify-content: center;
		align-items: center;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		background: rgba(0,0,0, 0.8);
		z-index: 100;
		pointer-events: none;

	}
</style>


<?php
	//the_archive_title( '<h1 class="page-title">', '</h1>' );
	//the_archive_description( '<div class="archive-description">', '</div>' );
?>
<?php 
	$obj = get_queried_object();
	$cate_name =  $obj -> {"category_nicename"};
?>


	<?php
		// $latest_tags_count = get_field('latest_tags_count');
		// $hot_tags_count = get_field('hot_tags_count');
		$cur = get_query_var('paged');

		$the_query = new WP_Query(array(
			'posts_per_page'=>6,
			'post_type' => 'act',
			'paged' => $cur
		));

	?>


<section class="container content-wrapper">
  <div class="row">
  <!-- sidebar -->
	<?php 
		$related_posts = get_field("related_act", 187);
	?>
	<?php set_query_var( 'related_posts', $related_posts ); ?>
	<?php get_template_part("template-parts/content", "sidebar"); ?>
<!-- sidebar -->


    <div class="col-md-8 col-12 order-md-2 order-1">
      <div class="row">


		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>



			
				
				<!-- <div>
					<?php //echo get_the_date( 'Y' )-1911; ?>
					<?php //echo get_the_date( 'm' ); ?>
					<?php //echo get_the_date( 'd' ); ?>
					<?php //the_title( '<div>', '</div>' ); ?>
					<br>
					
				</div> -->
				<!-- <a href="">
					了解更多
				</a> -->
				
		<div class="col-lg-6 col-12 activities-items">

          <div class="card">
            <div class="card-img <?php echo (!comments_open())?"end":"";?>">
              <a href="<?php echo get_permalink() ;?>">
			  	<?php 
					$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
					if(!empty($post_thumbnail_id)) :?>
					<?php $img_ar =  wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
					<?php $img_alt =  get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>
					<img src="<?php echo $img_ar[0];?>"
						alt="<?php echo $img_alt;?>"
					/>
				<?php endif; ?>
              </a> 
            </div>
            <div class="card-body">
              <h5 class="card-title text-secondary">
			  	<?php echo get_the_date( 'Y' )-1911; ?>年
				<?php echo get_the_date( 'm' ); ?>月
				<?php echo get_the_date( 'd' ); ?>日
				/滿額<?php echo get_field("total") ?>人</h5>
              <p class="card-text">
							<?php echo wp_trim_words(get_the_title(), 20, '...'); ?>
			  </p>
              <a href="<?php echo get_permalink() ;?>" class="btn btn-primary ">暸解更多<i class="fas fa-arrow-right"></i></a>
            </div>
          </div>
        </div>


		<?php
			endwhile;
			wp_reset_postdata(); 
		?>


        








      </div>
    </div>
  </div>



<nav aria-label="Page navigation example ">
	<?php my_pagination(); ?>
</nav>

  <!-- <nav aria-label="Page navigation example ">
    <ul class="pagination justify-content-center">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <li class="page-item active"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">4</a></li>
      <li class="page-item"><a class="page-link" href="#">5</a></li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav> -->
</section>

<?php
//get_sidebar();
get_footer();
