<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package ken-cens.com
 */

$page_name = "協會新聞";
$middle_pages = json_decode('[

]');
$page_title = "協會新聞:搜尋";
get_header(); ?>

<?php set_query_var( 'page_name', $page_name ); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php get_template_part("template-parts/content", "breadcrumb"); ?>

<?php 

?>

<section class="container-fluid content-wrapper ">		
	<article class="container content-wrapper news-list-category">  
		

		<?php

			//$per_page = get_field('hot_per_page');
			$cur = get_query_var('paged');
			$args2 = array(
				's' =>$s,
				'posts_per_page'=>5,
				'post_type' => 'post',
				//'paged' => get_query_var('paged'),
				'paged' => $cur
			);

			$the_query = new WP_Query($args2);

			
		?>
		<?php
		_e("<h2 style='font-weight:bold;color:#000;margin-bottom:30px;'>搜尋: ".get_query_var('s')."</h2>");
		; ?>
		<?php while ($the_query -> have_posts()) : 
			$the_query -> the_post(); 
		?>

		
		


	<div class="row news-info-items">
		<div class="col-md-1 col-2 my-auto px-0">
			<div class="news-date">
			<div class="new-num text-center bg-secondary ">
				<h6 class="mb-0"><?php echo get_the_date( 'd' ); ?></h6>
			</div>
			<div>
				<div class="new-mon text-center bg-primary text-nowrap">
				<h6><?php echo get_the_date( 'M' ); ?></h6>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-11 col-9 px-0 news-info-line">
			<div class="row">
			<div class="col-10 px-0 ">
				<div class="news-list">
				<div class="news-list-title">
					<a href="<?php echo get_permalink() ;?>"
					post_id="<?php echo get_the_ID() ;?>"
					class="news-link"
					>
					
					<h5><?php the_title(); ?></h5>

					
					</a>
					
					
				</div>
				</div>
			</div>
			<div class="col-2 my-auto news-info-right">
				<a href="#" class="">＞</a>
			</div>
			</div>
		</div>
	</div>

		

			
	<?php
		endwhile;
		wp_reset_postdata(); 
	?>

		

		<nav aria-label="Page navigation example ">
			<?php my_pagination(); ?>
		</nav>


	</article>



</section>

	

	<!-- <nav aria-label="Page navigation example ">
		<?php //my_pagination(); ?>
	</nav> -->


</article>

</section>

	

<?php
//get_sidebar();
get_footer();
