<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?>




	


<footer class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-6 col-12 p-0 association">
          <h4><?php echo get_field('footer_info_title', 224); ?></h4>
          <ul>
			<?php if( have_rows('footer_infos', 224) ): ?>
				<?php while( have_rows('footer_infos', 224) ): the_row(); ?>
					
					<li><?php echo get_sub_field('info'); ?></li>

				<?php endwhile; ?>
			<?php endif; ?>
          </ul>
        </div>
        <div class="col-lg-7 col-12 mb-5">
          <p class="footer-title">聯絡我們</p>
			<!-- ////////////////// -->


          <!-- <form action="">
            <div class="row">
              <div class="col-6" style="padding-right:3px">
                <input type="email" name="email" placeholder="信箱" required>
              </div>
              <div class="col-6" style="padding-left:3px">
                <input type="text" name="title" placeholder="主旨" required>
              </div>
              <div class="col-12" >
                <textarea name="text-content" id="" placeholder="請輸入內容" required></textarea>
              </div>
              <div class="col-sm-3 col-4 ml-auto">
                <button type="submit" class="btn btn-primary w-100">送出 <i class="fas fa-arrow-right"></i></button>
              </div>
            </div>
          </form> -->

		<?php echo do_shortcode('[contact-form-7 id="229" title="footer表單"]'); ?>





		  <!-- ////////////////// -->
        </div>
      </div>
    </div>
    <div class="row bg-primary copyright">
		<?php echo get_field('copyright', 224); ?>  <span class="d-inline-block pl-1"> All right reserved 2018</span>
    </div>
    <div class="gotop"><i class="fas fa-angle-up"></i></div>
</footer>
<style>
	.footer-submit-wrap {
		padding: 0 !important;
    min-width: 100px;
    max-width: 150px;
    
	}
	.footer-submit-wrap input {
		padding: 8px 12px !important;
		border: none;
		background: none;
		color: inherit;
		padding: 0;
		margin: 0;
		width: auto;
		width: 100%;
		cursor: pointer;
	}

	.footer-submit-wrap .fas {
		position: relative;
		left: -50%;
	}
  @media (max-width: 768px) {
    .footer-submit-wrap .fas {
      /* left: -60%; */
    }
  }


	* {
		/* border: 1px solid ; */
	}
    
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="<?php bloginfo("stylesheet_directory"); ?>/src/dist/js/all.js"></script>


<?php wp_footer(); ?>

</body>
</html>
